package com.yves.ph.AppManager;

import com.yves.ph.ComponentFactory.ComponentBuilder;
import com.yves.ph.ComponentFactory.ComponentFactory;
import com.yves.ph.FileManager.FileManage;
import com.yves.ph.FileManager.FileManager;
import com.yves.ph.IdGenerator.IdGenerate;
import com.yves.ph.IdGenerator.IdGenerator;
import com.yves.ph.Messenger.Message;
import com.yves.ph.Messenger.Messenger;
import javax.swing.*;
import java.awt.*;

public class AppManager implements AppManage{

    private FileManage fileManager;
    private IdGenerate idGenerator;
    private Message messenger;
    private ComponentBuilder componentFactory;
    private String prevComm;
    private String currComm;
    private JFrame window;
    private JPanel currPanel;

    public AppManager(){
        init();
        listenCommands();
    }

    private void init(){
        this.prevComm = "";
        this.currComm = "";
        this.fileManager = new FileManager();
        this.idGenerator = new IdGenerator();
        this.messenger = new Messenger();
        this.componentFactory = new ComponentFactory(this.fileManager.getAppSettings(), this.messenger);

        this.window = componentFactory.createWindow();
        this.currPanel = componentFactory.createHomePanel();
        this.window.add(currPanel);
        this.window.pack();
    }

    public void listenCommands(){
        while (Thread.currentThread().isAlive()) {
            currComm = messenger.getMessage();
            if (!prevComm.equals(currComm)) {
                setScreen(currComm);
                prevComm = currComm;
            }
        }
    }
    public void setScreen(String comm){
        switch (comm) {
            case "HOME":
                replaceScreen(componentFactory.createHomePanel());
                break;
            case "EXIT":
                System.exit(0);
                break;
        }
    }

    public void replaceScreen(JPanel panel){
        this.window.remove(currPanel);
        this.currPanel = panel;
        this.window.add(currPanel);
        this.window.pack();
        this.window.repaint();
    }

    public void setTheme(Color back, Color front){
        if (back != front){
            if (back != null && back.getRGB() != this.fileManager.getAppSettings().getFrontColor().getRGB()){
                this.fileManager.getAppSettings().setBackColor(back);
                this.window.getContentPane().setBackground(this.fileManager.getAppSettings().getBackColor());
            }
            if (front != null && front.getRGB() != this.fileManager.getAppSettings().getBackColor().getRGB()){
                this.fileManager.getAppSettings().setFrontColor(front);
            }
            this.fileManager.saveSettingsFile();
        }
    }
}
