package com.yves.ph.AppManager;

import javax.swing.*;
import java.awt.*;

public interface AppManage {

    public void listenCommands();

    public void setScreen(String comm);

    public void replaceScreen(JPanel panel);

    public void setTheme(Color back, Color front);
}
