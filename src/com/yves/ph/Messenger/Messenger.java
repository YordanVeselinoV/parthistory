package com.yves.ph.Messenger;

public class Messenger implements Message{

    private String message;

    public Messenger(){
        setMessage("");
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
