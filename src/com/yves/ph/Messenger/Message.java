package com.yves.ph.Messenger;

public interface Message {

    public void setMessage(String message);

    public String getMessage();
}
