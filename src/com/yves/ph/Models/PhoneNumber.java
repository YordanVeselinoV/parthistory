package com.yves.ph.Models;

public class PhoneNumber {

    private final String id;
    private String number;

    public PhoneNumber(String id){
        this.id = id;
        setNumber("");
    }

    public String getId() {
        return this.id;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
