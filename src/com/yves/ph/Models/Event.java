package com.yves.ph.Models;

import java.util.ArrayList;
import java.util.List;

public class Event {

    private final String id;
    private String date;
    private List<String> partIds;

    public Event(String id){
        this.id = id;
        setDate("");
        setPartIds(new ArrayList<>());
    }

    public String getId() {
        return this.id;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getPartIds() {
        return this.partIds;
    }

    public void setPartIds(List<String> partIds) {
        this.partIds = partIds;
    }
}
