package com.yves.ph.Models;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private final String id;
    private String name;
    private List<String> phoneNumIds;
    private List<String> vehicleIds;

    public Customer(String id){
        this.id = id;
        setName("");
        setPhoneNumIds(new ArrayList<>());
        setVehicleIds(new ArrayList<>());
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhoneNumIds() {
        return this.phoneNumIds;
    }

    public void setPhoneNumIds(List<String> phoneNumIds) {
        this.phoneNumIds = phoneNumIds;
    }

    public List<String> getVehicleIds() {
        return this.vehicleIds;
    }

    public void setVehicleIds(List<String> vehicleIds) {
        this.vehicleIds = vehicleIds;
    }
}
