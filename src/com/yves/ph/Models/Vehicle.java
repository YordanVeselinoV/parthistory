package com.yves.ph.Models;

import java.util.ArrayList;
import java.util.List;

public class Vehicle {

    private final String id;
    private String vehicleInfo;
    private List<String> eventIds;

    public Vehicle(String id){
        this.id = id;
        setVehicleInfo("");
        setEventIds(new ArrayList<>());
    }

    public String getId() {
        return this.id;
    }

    public String getVehicleInfo() {
        return this.vehicleInfo;
    }

    public void setVehicleInfo(String vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    public List<String> getEventIds() {
        return this.eventIds;
    }

    public void setEventIds(List<String> eventIds) {
        this.eventIds = eventIds;
    }
}
