package com.yves.ph.Models;

public class Part {

    private final String id;
    private String partNum;
    private String partName;

    public Part(String id){
        this.id = id;
        setPartNum("");
        setPartName("");
    }

    public String getId() {
        return this.id;
    }

    public String getPartNum() {
        return this.partNum;
    }

    public void setPartNum(String partNum) {
        this.partNum = partNum;
    }

    public String getPartName() {
        return this.partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

}
