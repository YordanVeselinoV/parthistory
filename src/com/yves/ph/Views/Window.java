package com.yves.ph.Views;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame {

    public Window(){}

    public void setBackGround(Color back){
        this.getContentPane().setBackground(back);
        this.repaint();
    }

}