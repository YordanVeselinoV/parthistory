package com.yves.ph.Views;

import com.yves.ph.ComponentFactory.ComponentBuilder;
import com.yves.ph.Messenger.Message;
import com.yves.ph.Settings.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomePanel extends JPanel {

    private Settings settings;
    private ComponentBuilder componentFactory;
    private Message messenger;

    private JPanel topPanel;
    private JLabel titleLabel;

    private JPanel centerPanel;

    private JPanel bottomPanel;
    private JButton exitButton;

    public HomePanel(Settings settings, ComponentBuilder componentFactory, Message messenger){
        this.settings = settings;
        this.componentFactory = componentFactory;
        this.messenger = messenger;
        this.setPreferredSize(new Dimension(this.settings.getWIDTH(), this.settings.getHEIGHT()));
        this.setBackground(settings.getBackColor());
        this.setLayout(new BorderLayout(5, 5));
        this.setBorder(BorderFactory.createLineBorder(settings.getFrontColor(), 2, true));
        init();
    }

    private void init(){
        this.topPanel = this.componentFactory.createJPanel(this.settings.getWIDTH(), 100, this.settings.getBackColor(), this.settings.getFrontColor(), new FlowLayout());
        this.titleLabel = this.componentFactory.createJLabel(this.settings.getTitle(), 120, 50, this.settings.getBackColor(), this.settings.getFrontColor(), settings.getFont());
        this.topPanel.add(titleLabel);

        this.centerPanel = this.componentFactory.createJPanel(this.settings.getWIDTH(), 400, this.settings.getBackColor(), this.settings.getFrontColor(), new FlowLayout());

        this.bottomPanel = this.componentFactory.createJPanel(this.settings.getWIDTH(), 100, this.settings.getBackColor(), this.settings.getFrontColor(), new FlowLayout());
        this.exitButton = this.componentFactory.createJButton("ИЗХОД", 100, 40);
        this.exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                messenger.setMessage("EXIT");
            }
        });
        this.bottomPanel.add(exitButton);

        this.add(topPanel, BorderLayout.NORTH);
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);
    }
}
