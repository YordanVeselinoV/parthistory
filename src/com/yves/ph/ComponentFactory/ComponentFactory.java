package com.yves.ph.ComponentFactory;

import com.yves.ph.Messenger.Message;
import com.yves.ph.Settings.Settings;
import com.yves.ph.Views.HomePanel;
import com.yves.ph.Views.Window;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Base64;

public class ComponentFactory implements ComponentBuilder{

    private Settings settings;
    private Message messenger;

    public ComponentFactory(Settings settings, Message messenger){
        this.settings = settings;
        this.messenger = messenger;
    }

    @Override
    public JFrame createWindow() {
        JFrame window = new Window();
        window.setTitle(this.settings.getTitle());
        window.setSize(new Dimension(this.settings.getWIDTH(), this.settings.getHEIGHT()));
        window.getContentPane().setBackground(this.settings.getBackColor());
        window.setLayout(new FlowLayout());
        window.setResizable(false);
        window.setVisible(true);
        window.setLocationRelativeTo(null);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BufferedImage image = null;
        try {
            byte[] imageByte;
            imageByte = Base64.getDecoder().decode(this.settings.getImageString());
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        window.setIconImage(new ImageIcon(image).getImage());
        return window;
    }

    @Override
    public JPanel createHomePanel() {
        return new HomePanel(this.settings, this, this.messenger);
    }

    @Override
    public JPanel createJPanel(int width, int height, Color back, Color front, LayoutManager layout){
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(width, height));
        panel.setBackground(back);
        panel.setForeground(front);
        panel.setLayout(layout);
        return panel;
    }

    @Override
    public void setJPanelBorder(JPanel panel, Color front, int thickness, boolean rounded){
        panel.setBorder(BorderFactory.createLineBorder(front, thickness, rounded));
    }

    @Override
    public JLabel createJLabel(String text, int width, int height, Color back, Color front, Font font){
        JLabel label = new JLabel();
        label.setPreferredSize(new Dimension(width, height));
        label.setBackground(back);
        label.setForeground(front);
        label.setFont(font);
        label.setText(text);
        return label;
    }

    @Override
    public JButton createJButton(String text, int width, int height){
        JButton button = new JButton(text);
        button.setPreferredSize(new Dimension(width, height));
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
        return button;
    }

    @Override
    public JTextField createJTextField(String text, int width, int height, Font font){
        JTextField textField = new JTextField();
        textField.setBackground(settings.getWhite());
        textField.setForeground(settings.getBlack());
        textField.setFont(font);
        textField.setText(text);
        textField.setPreferredSize(new Dimension(width, height));
        return textField;
    }

    @Override
    public JPopupMenu createJPopupMenu(int width, int height){
        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.setPopupSize(new Dimension(width, height));

        Action copy = new DefaultEditorKit.CopyAction();
        copy.putValue(Action.NAME, "Копирай");
        copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));

        Action paste = new DefaultEditorKit.PasteAction();
        paste.putValue(Action.NAME, "Постави");
        paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));

        popupMenu.add(copy);
        popupMenu.add(paste);
        return  popupMenu;
    }

    @Override
    public JTextArea createJTextArea(String text, Font font, boolean editable){
        JTextArea ta = new JTextArea();
        ta.setBackground(Color.WHITE);
        ta.setForeground(Color.BLACK);
        ta.setFont(font);
        ta.setText(text);
        ta.setEditable(editable);
        ta.setEnabled(true);
        return ta;
    }

    @Override
    public JScrollPane createJScrollPane( int width, int height, JTextArea ta){
        JScrollPane sp = new JScrollPane(ta);
        sp.setPreferredSize(new Dimension(width, height));
        sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        return sp;
    }

    @Override
    public JRadioButton createJRadioButton(String text, int width, int height, Color back, Color front, Font font){
        JRadioButton radioButton = new JRadioButton();
        radioButton.setBackground(back);
        radioButton.setForeground(front);
        radioButton.setPreferredSize(new Dimension(width, height));
        radioButton.setFont(font);
        radioButton.setText(text);
        radioButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        return radioButton;
    }
}
