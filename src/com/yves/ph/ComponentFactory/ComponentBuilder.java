package com.yves.ph.ComponentFactory;

import javax.swing.*;
import java.awt.*;

public interface ComponentBuilder {

    public JFrame createWindow();

    public JPanel createHomePanel();

    public JPanel createJPanel(int width, int height, Color back, Color front, LayoutManager layout);

    public void setJPanelBorder(JPanel panel, Color front, int thickness, boolean rounded);

    public JLabel createJLabel(String text, int width, int heigth, Color back, Color front, Font font);

    public JButton createJButton(String text, int width, int height);

    public JTextField createJTextField(String text, int width, int height, Font font);

    public JPopupMenu createJPopupMenu(int width, int height);

    public JTextArea createJTextArea(String text, Font font, boolean editable);

    public JScrollPane createJScrollPane( int width, int height, JTextArea ta);

    public JRadioButton createJRadioButton(String text, int width, int height, Color back, Color front, Font font);

}
