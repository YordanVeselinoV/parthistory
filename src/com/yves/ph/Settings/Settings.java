package com.yves.ph.Settings;

import java.awt.*;

public interface Settings {

    public String getAppDir();

    public void setAppDir(String appDir);

    public String getIdGeneratorFileName();

    public void setIdGeneratorFileName(String idGeneratorFileName);

    public String getSettingsFileName();

    public void setSettingsFileName(String settingsFileName);

    public String getCustomerFileName();

    public void setCustomerFileName(String customerFileName);

    public String getEventFileName();

    public void setEventFileName(String eventFileName);

    public String getPartFileName();

    public void setPartFileName(String partFileName);

    public String getPhoneNumFileName();

    public void setPhoneNumFileName(String phoneNumFileName);

    public String getVehicleFileName();

    public void setVehicleFileName(String vehicleFileName);

    public String getFirstSeparator();

    public void setFirstSeparator(String firstSeparator);

    public String getSecondSeparator();

    public void setSecondSeparator(String secondSeparator);

    public String getThirdSeparator();

    public void setThirdSeparator(String thirdSeparator);

    public String getFontCode();

    public void setFontCode(String fontCode);

    public String getTitle();

    public void setTitle(String title);

    public String getImageString();

    public void setImageString(String imageString);

    public int getWIDTH();

    public void setWIDTH(int WIDTH);

    public int getHEIGHT();

    public void setHEIGHT(int HEIGHT);

    public Color getBlack();

    public void setBlack(Color black);

    public Color getWhite();

    public void setWhite(Color white);

    public Color getGreen();

    public void setGreen(Color green);

    public Color getYellow();

    public void setYellow(Color yellow);

    public Color getRed();

    public void setRed(Color red);

    public Color getBlue();

    public void setBlue(Color blue);

    public Color getGray();

    public void setGray(Color gray);

    public Color getOrange();

    public void setOrange(Color orange);

    public Color getBackColor();

    public void setBackColor(Color backColor);

    public Color getFrontColor();

    public void setFrontColor(Color frontColor);

    public Font getFont();
}
