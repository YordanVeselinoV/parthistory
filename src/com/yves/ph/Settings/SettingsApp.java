package com.yves.ph.Settings;

import java.awt.*;

public class SettingsApp implements Settings{

    private String appDir;
    private String settingsFileName;
    private String idGeneratorFileName;
    private String customerFileName;
    private String eventFileName;
    private String partFileName;
    private String phoneNumFileName;
    private String vehicleFileName;
    private String firstSeparator;
    private String secondSeparator;
    private String thirdSeparator;
    private String fontCode;
    private String title;
    private String imageString;
    private int WIDTH;
    private int HEIGHT;
    private Color black;
    private Color white;
    private Color green;
    private Color yellow;
    private Color red;
    private Color blue;
    private Color gray;
    private Color orange;
    private Color backColor;
    private Color frontColor;

    public SettingsApp(){
        init();
    }

    private void init(){
        this.appDir = "C:\\PartHistoryApp";
        this.settingsFileName = "appSettings.txt";
        this.idGeneratorFileName = "idRepo.txt";
        this.customerFileName = "customerRepo.txt";
        this.eventFileName = "eventRepo.txt";
        this.partFileName = "partRepo.txt";
        this.phoneNumFileName = "phoneNumRepo.txt";
        this.vehicleFileName = "vehicleRepo.txt";
        this.firstSeparator = "~";
        this.secondSeparator = "@";
        this.thirdSeparator = ">";
        this.fontCode = "arial" + thirdSeparator + "0" + thirdSeparator + "20";
        this.title = "Архив Части";
        this.imageString = "iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAIAAAAHjs1qAABue0lEQVR4XuxdB4BUNRPO7u1e4Y7eexVBpAn" +
                "SLGClCqIgKtIFpIjwq4AVQZGmqAgWOtgbKM1GU2k2FBURpQvSi9K5233/zHwvudzuHhx4FGHH5/IuL++9vOTLZDKZzKhglKJ" +
                "0YVAgEFChaVGK0nlKUbhH6QKiKNyjdAFRFO5RuoAoCvcoXUAUhXuULiCKwj1KFxBF4R6lC4iicI/SBURRuEfpAqIo3P8zFLA" +
                "oJN382idRCqco3P8bRO3kWER/pqSk4BL9efToUSQiWxTx6VEU7v8NMnCn8yNHjhDWLfBz4rFjx0LYfJTCKQr3/wyBeQeFnRN" +
                "Nnz69f//+gwcPnjdvHlKSk5OjWD8+ReH+HyOC9bKlS1Va8ng8v/zyC10yEk6UIlIU7v8NIr59JJkF9BmzZjK+fTH++FhfrJ+" +
                "OuIR4rz+GEqe8PtUROT6YnBLFfUSKwv2/QckirM+cNYtgHeP3+eNiY+Pj6IR+gXj6pUuLFi2ibMeOunJ8lEIoCvf/BjHWZzP" +
                "WY+MI3bGeGC9QTudAv9cXE58lgTJ89913TlQ5kw5F4X5uEeajIJNI8J08dQpBmQBNQAdHx685Iegrj0pMTKRsXy9Z6oi6Jko" +
                "hFIX7OURpIB4MHktJSQ6wDDPwyYGXVa/WqEljlmR8MQzxuFiCPvg6/YlfgD5GpJqFCxcC8Xb/sZ9/YVIU7ucW2biEvP78Cy+" +
                "ULF1KNI0OnbA8Ex/HvFypLEmJwDoOkmfMCV394osvgHjcC7rAER+F+7lLhM4Xx4wWTaP6fc0fwGvpi8rQn37h7jbWjShP6Xz" +
                "J56Nsq1evxl3mmVG4R+F+DpEwdv4hjI555WXMTaF1+WvbVmC3WIni9Gdi1iRGuRZjjBxPEg6f+3xZsmShbBs3bnREHx+w7A4" +
                "uWIrC/eyTll/EBCA5cPQI69dHDBvOXN3riU2IJ+GEQEx/7di1E4gvXrKEEo2kT3SR1CXQKwy/Zx1lXCzdTtk2b95Mtxw7dsy" +
                "G+4UJ/SjczzKFSxcEzYf6969Q/hI6YcDHeJOyJhGmGf1Kbd22DYhnqcbDUk1CYhaA3mAdgg11Eog3BvEhL7oAKQr3s0wG7jg" +
                "hUPbp04cACkzvP3iAuXisP0tilnhRNRJt2foXrlI3YHV7QrxZbzKiPHUSGhZ4Ruv1xMfzvBaIdy5sw8ko3M8OuaILZBiRqgO" +
                "ih/nfA/czor0eEs2BzgMHDyqxGgCmweYN4qGiAS8HX3eFeEoRDT2rJmNiMHPdsmWLI5ZkRKYMFxRF4X4WCMwVRrz8N50LX+8" +
                "tfJ0ACkznypMbmD5y+DAjPsYLQEOOxyUiEmaI69MtfrEsgAADTm/QT7dgzfXY0WO46wLEejAK97NCtizhChaOc1+f3kpU6ZC" +
                "5gc6kbC6P371vD3g8sOsTiRyLUEQQ0I3IHi8mNEawofzUJdAZTD8JLdOFQVG4nwWy4U5yBZ3f1bYN+DdB00gmLo/PlQsA3bl" +
                "zJ2M6xheXJSHGF0OZ6c+AXkVKypbViwVXusvrMVhHH6AUQnxsgjvfDcpdocW6ACgK9zNHqexc4B7USG1yUxMl81EjgWDeCZ0" +
                "6XcqeLRty/vH7H4zWGK9PeDZWT4+lcIch4oms1tUYSQbcHfp47gA+v9fL/QRvt8tmimcSzz+Kwv0MkQGTA91IMJgi581b3Kr" +
                "ELgAQhwgOgNo8vnTZMsD072vXMOAlA8YBxi6uQXFJd1En8ah4y17SyDb4k0QmyomBxZTKLmTasp8/FIX7mSOs7DgySU0Osth" +
                "9y623KD03BSKxosSq9Fg//QLWkOMrVq7kIv4P5vHoGx6vJ41ELohXYjvpFYEHjzKCDQE9TnckcxeBgKBvFp6icI9SJhBW8nF" +
                "OIGt+S3PwaRwQsmHqCP06k9fDicSnRaqpXLUKALpuw3q+yHua4kiOhwyDS44gnkUdwbpfhCIX7vRL/0pPiNPrVrjFrLmex1g" +
                "PRuF+hsl4EGgs8nqc2K+7UrUWryk9b768d7S+s3Wbu6pWuwyg5LUkQfyllSriCWvWrQWsibuzLlLsxnDJEcS7glBM6rSVES9" +
                "dy1XU+PmBNJyYu5zzff4ahftpJ/BL7B4NiJRds04tJfI6EAkxwytE6T+s+NHG36HDh6+uV5f7ht6hZ6SauXPnck8gbi0rSoo" +
                "Vl1nNjUq2bMfFsixkpq0u6LU+Hkob6gC4haWaY7wCdb5SFO6nl1hMlxkh0dEUXuKpXuNyw3pxMFMXG8b4+Hjk3PTnn48+/tj" +
                "9Dz4wb8F8pLS4rSXdlZiUGCvLqDVq1UT6Z59/Tn8m6K0eGCtwae/evdypYnk+AEHfHUYs0HNni/WzTtOjpZojx0K/4TyiKNx" +
                "PO1EVk2TsiLxOMGV0yvTRHGDM2bNnB+BGPvecsqj2FbWR3uqO25X0E/D4xo0bI/3LRV8p0dmzSCMmkL5YF/Hbt29XEPHDVDQ" +
                "4aFhIzJY1RpZpFeT4wPksz0ThftoJxjBEl9dkvk5SOBZHjVBBnNWnWfKQYUM4T5YEg0j68/KaNXD1tla3KS2v00mzZs2Q/tl" +
                "c5vE+WXDlWzyqeLFiuLRzBy9OYSps7wjBSaxoaeKEx1O2QoUK4S4u9vk4Z43C/bQQbLD413GgcwRfN2ijSaoPdizClQGyIcO" +
                "GUh6/KCUNMwYvv6x6NeSpVac2+gMYecMmjZA+b+48vlcmo37pDAUKFcSl7Tt3KMuMjJm6LyZW7+zmW4T384u8HhjqBGUsOv8" +
                "QH4V75lNACC7sADjsxjBslUGmZZi8+fIiz9Dhw+w8ODF/KksLSdBXeqMqnTS6qQnSJ06ahN7iYlepEqVK4tLW7duUdlDDJsG" +
                "RNv5BuKdsPbp3d8RwkmSw0G/7j1MU7plPLKlrg0eiPPnyKpFh0sBdpBSz5/qhRx7mPGlRaMMxMWsSI7uxy8urXFZVyRwAAL3" +
                "pppuQPm7iePQZMG86L1TYlU+2bP2L/mTer2UY+/nAOoE+SxLrNJ3zVJ6Jwj3zCdwdICtYuJDSOkQDdGC9cJHCyHNP927oDzF" +
                "6vSkE65wikgZla3azK6+Dx/tFDqGTzp07I33qa6+h57BQLmqcIsWK4NLyH35QWvRnnYz1/FhZuzXDwtdff+2cjx4no3DPTMJ" +
                "S/DESY3g538U6YETAglmvT9BZtLg7lezYuZMSHTxQbmM9zbnW0FPmm29pjnuNTtMTwz2hc9cuSB8/cYL7TJoeCOIrVLwUl77" +
                "/YTkQD1EerwiR4ynDggULHFlqDf3C/zhF4Z45BAMBhnuyaw9DQjnW9iE2ELx4f4bI60WKFQX4una7B7g0cAd/taFv0nEJDLt" +
                "Z85vxhJKlSipLqqGBAumTpkzGk4FsZal3fl75CxAf/jqfnlH8+uuvjojvod/5H6co3DONDh8+7GiRPWv2bErkE0BZCcXGMuM" +
                "sUbw4YNe5Sxdl7VFCTiNmAI44fKKoATR5B6ow4KZaqoEfjljta6nLPV2R/srYV7kMMh+IEyOzmrVr4dIPK35UIsfDmsB0qli" +
                "R6b0ej3M+qmWCUbhnFgW0FxcHNomi/fCKdS6g+d60D3Lnzq20SVa3HiyvE9pC4A5mDFkfl8yfBpeUDmS37dAeTytStIgShs0" +
                "KSqW69+yB9CcGDaQ//SL64xaD+BU//8TFFEeqXjHDpAywvZkzZw7ynH+Ij8L9XxEgTr9HRRvjaPtbGKLQvy1atDh48CAuUbb" +
                "BgwfTydixY5XMX80Ulv36CqCBV6KLy5erXLUKzuEXkhFvSfN+4fH/e+B+PLxYSZfH470PP/oI0oF4wNon09Bql1fHpY9mzlC" +
                "yFOAVU2FcHT9ponM+Ah0UhXsmEFXi0WSGuzfGyxvnBDe58+bZtPlPAAuBk4zxTKGCBY3o7DL1GG9i1iQ25VXq+VEvIBto0uT" +
                "JjHhMLrX7X3B97NLoeW9P5MxfqKCSEQM94dlnn0X6o48/pmBVH+tPkFuuqns1LsGJtjhvYolr7ITxzvmokDEUhfupE1ig0Tk" +
                "qEcR9Atmhw4Yh0bBJKG1MTmhF4vSGI1gNULrpISnBAFyiEtEJ36KzGcRD1KZL9z/4AHIymr0evioThqee5sGEaNCTg5Rew8K" +
                "LWt1xOy7NmjOHEU98fTxj3Uhl5yVF4X7qBKADxKUvKg3QFCpSeM++vUDSsRReWGXTX8kTgK2Y41xUpgx1DOjFMQfFgDB0xHC" +
                "6mhxgP9d0EOL5CTIfmPo6a9OxFmvkeL82B6BL/R9+iLKt/uN3hZAH2vrg6SFPozADBj7Bl2TWy/70lLqlxa24NHnKlJEjR/K" +
                "r9XY+fF3azz0fKAr3UyRgHWimk+HD2aXjsKHM1IPpeGA0wszXy75W2g4M2PX42NLdCbrR8xx27+4aDzPsMQP2uNpDyPGuNC9" +
                "/Qqr5Tfz9FitejDuGDBqYng7WiO/clXVBuAT1f4vbWuKSo2UY/J6XWA9G4X7KZOBuzkFBC+shoCHWbgxpJk1m+xbWHsoilMc" +
                "XUyB/AYO5gJD9BLpUuXIlA3ewdgN3MHiINHd36az0Oi50mvTnqBdHoXht27fjniYaUnQGLKCi39qlPS8pCvdTJxuRgMuRI0c" +
                "M9M0lm7AlFBkUZH1h0iST5MvLtmI23IFCPIp+K1WuZPBt4A7ZBnDv1oNNu+69r5eBuxkKKOWD6dPw3nYd24PH0+8CCXlgQhD" +
                "jjSFlPp8oCvd/ReEowcI78GrnDGL/nmD9sDjBI+Eka/ZsrFvUM85UYYYeGHR9RwatqTDUODbc4YsGfHrJMg7JVE0sCwx3xwF" +
                "wv/v+e3hU+44d6M/P5811tOt3lDkK9yidgMKRHfKn0UJCWUlwVqKZYVcwsqyjRPmttCIlCClfzG5oqsqdSRsFkIwOeSaVtdN" +
                "MN5al8CZN2Az48NEjyGazdsrm7nWyePyKn34C1m0Z5vzGejAK99NNUMUA8XySksxYFwdgSdmy0vmCL7948qknlZYulsu+bL4" +
                "zJXjg8EFgffNfW9wuIV5lAGK/ZeXbtGlTgBiGwYb92wf1E1gRjx0/DpntjnreAx0UhftpJ6OQOXLsGGNdloqw9jn7Y16u79C" +
                "po9L76+jk6addRQpo2IgRBuscoEYLMH5ZaaJLjZu6xu7XXHcNdxuZg9qIx2IWdydxX7N46RJHHDDZhYzCPUqZQCnaE4Err8P" +
                "pl8DuF7E6vLxmDUaz9hzmi2PEExXMlz9fgXw4Z2QLI+c9InRiaWMa3eRu0L766quVNiKAGAOs813amw1lWLdhvSODzFFrjnG" +
                "BYD0YhftpIsjEAW1ccPDIIQA3TsyA6WTV6t8ovdwl5ZWWPdAHSJrH6mmMn822/FpiAXsGR4drJMrctJkrw9S56ko8B48yWDd" +
                "PQN9YK1gHuM3JBUVRuGc+wUzc6Bx37GJfALB6B6Z//W0VpZevcAmn60kkSSk/aitFzhy2lgRpByBWljnk1dew0yX0CqCc88h" +
                "DaM7q12FW165fR5lTtC/IKNyjlDkUsPZl//nXZoYyTNhFNN+3/x9Kz5Erp9JGkUoEd2B35+5dsKaM1e5RAV8bypS/cxd3q95" +
                "FZS9S4jwMkfdMHp9MTH2i0acM6zduYKwHWLVpUB6Fe5QygahOjx3lYJGbNm0C2zZmvXv/+ZvS8+ZnoRwTSj6JjwN2QaUvKgP" +
                "xw3B34DhOh/Qw5uwIKUyDg+kYZihgqUmCGlCGnbt3OzI3TQ5eQDrHiBSFe+YQpqTAEOT1P7cwXwcKIX4Ao4AgM3URbGI11qF" +
                "zzJYjOzoGzGDcvUg6tp7SS6dERYoVVZa8jgM5Y0TRib7x9wEeTIStn/82AiekKNwzh1LlddHDQFMOm0fMTYFRJYYDBFyPyBh" +
                "Zs7thOcxVuBsoVrI4/dnxbnfXNhCsmK+7WMeGPRvofhF+6OG8FEXMXp6PjgesR+EejMI9swh6GOgcwdddVbcllysvdwCjJyk" +
                "ojjfA1PmqMHuiWnXYKWRAZpNDhw5FVFRfXOxrb76OnODrYOQG6z7R3rBgo70BHznGAhVNIIJabIkiPgr3zCEWYzA3FXndYNr" +
                "IKoAytCvKckZgrmIQMM6sjyS7C7EhlL9gASWa+DQTUznnviSSD2XYf/AA8iNyE6SsKNyjcD91CmgiAQbM+K9tW5XeqaQsb+v" +
                "AOsvZIpMUL1nCkT1K9lVl7SLFWAHCuyixZ8+eyAaIY4M2ZHfm9FDeCx06wj4RzL3mJEqBKNxPmSC6wCrG0TuJjJsAYHrTn38" +
                "CoFlk/V9ZjvKI9h/iGPAQYypVqYxEPDBorVUFBe6TJ09W4r3DQBxzAxoxsOYKrKcE0xg2Rjm6TVG4nyIFRNI4cuQI0Pnl4q8" +
                "AXMgwpcqUpsT1Gzco7XoXfQB+LyCvHzjE0d9drIsMY2QXgNXGOrrWBx98wIi3PDzGyBIsXHQoPSFOMdJ6FO5pKQr3UyGgkE3" +
                "bBfSLFi1i3MZ4E0T3V+HSCpS4avVvdJ6QlOgL8+NFdFTMxaCHue6G65F4jF2QudrMkNcF9C7BN998U2kFpU9vPMUCFh7Ce7o" +
                "D5/MGvH9DUbifHEHAcIQBHxOOu2jJYsat1ovXufIKSlz45ReMdQKlzxcTwzrB2rXdIBxENI9UYhZGv02bszMwjBX2iwBxA1k" +
                "D9w0bNijZfB0n4fiMDIMng5fjgbjLfmaUonA/OToqy6X0C/364qVLGHyyf4JO6l57DSUu+GIhEjkQto8xXb9+fUdcDNDv2vX" +
                "rlLZuN64e8fDjoJMukeBEOVu2aMFdRVZq4VDAYN1A3JxHKYSicD85StGrp44OisSLlyJ/t2nbhhJff/MNxjoU4eKrqHXr1ow" +
                "/4c3GMQb9NhE7dX6WcO7jwxSARqwlqB3B13PkyolL4OtRpn58isI9Q2QAZHAJeZ2ZtMjfHTp1pMTJr01ROtIY5PU7br8DWKT" +
                "fNes5EipgCqwTHRaeHfKWEKIMhw4dot+iRXmBibEuglDe/LybO6j34OE56T0kSsEo3DNC0L1AN5Js8XW23ZW5KdxMv/jSGOb" +
                "cEsYamL79dvbUBRkGoQQA0zta3+lAXrf4cThMKYWlJr1BZNgIdmUDPQ+d5MmTR/qL21XwwPCHRMmmKNxPTMkIUqD3myLMHWu" +
                "7BesDBj5BiU8OforOswhfh6xy9913OzqW6ryF84FU+u3Z617AFOjESegrhQLidQx5du3ZzXxdGyZky+FGpbQzW7dGKTJF4X5" +
                "iMlyW4PWaiObQedPJwCcHMdZlbzV1gCxZk6Cf6du/H3DsmEi/socD7kvZJ56OFHB8mJo1rOw5smPPBz0kp8jrQW2XFqWMUxT" +
                "uGSLA7vXXX1fa5pZORr80hhJ79b5PieFAosb6Q4884ujF0U/nfmaw3rtPb0o5eJilcDwWWE8P8UePHAmKlEIDhdKDQ8Uq7oI" +
                "UsI5OFYwuJ2WMonCPTJj8BURnckz4+hQ4JdUu2CeIH/T2HdqrtDa6D/brS+lHRF85a85sXKXfHr2Yr7tPt/CdHtbZq5Isvxp" +
                "dp7IMDVJzpXd7lCJRFO6RyfBdyDCTp7LKhW1rhcVOnDSJEtuJv0W/dhNA54OeetLRRojjJ3A8MKT3uf9/NkYzSrJEarB+cfl" +
                "yIViP0slSFO4RCDL3USFHq9IJuJBJPvn0U0q86uqrlAgYMFyh8xfHjOa7RIYh3k8pcGM06sUXHdknGvqaExHd9d4H7yuhSyt" +
                "VpD8PHT4s2pzQnFHKIEXhHplIMgbWp06dqiRcDFgs/Cpec921YLpGthknsQBgVjBBolcjGhnke2LSJN6EvuNEhP0Zd3fpfGX" +
                "dqxzZacoe38OmpyHyjPmTvUyGpadeTUt2zvOYAlG4G4KG+5i4pYayfOob7tw0TrC+/McfKLFqtcu4A/h9qfGMJk5gvi46xxd" +
                "GjVLiLkZpfs+PPnk4BbQVjSOEP3ESmtUCsZmwBpLFLFJigJh0ZMO0xL1TU8THnn8UhXsqBRAYlXkog+ytd95mFp4Qj32lG//" +
                "cRInFS5ZgNGdJMOFlXnud99RBhnl84ADuHpI+5pWXGJ2Wf93Q9x2XbAQbsIY/xFxCNuSRTwgEU5i/03Q3Rdu0mavmxFDIY89" +
                "XCkThbihgRQt79vmRjHW9rLP5ry2UWLJ0KTpPTEqEKobOP/70E0dkFUdHg4G68OVXX2GEyaopHhgIQ+oJydwSsFajNGhdCmr" +
                "2f7JkvyioO9V5D/1AFO6GSIw5KhYsAwcOJMgiphf9C4UgHPbC9guYni3xRyGvD5RYX5Dv33nvXUf25gE4gBegecoELXtAdx6" +
                "bli1d9vprr/2++vcNGzb06dPniiuuqH/jjS+9+jLNsBs2blSjdq0ePXp89+13W7dunTRp0rx580wPNJSSlt+Hvvs8ogsd7pB" +
                "eArJuCmA+/oQIJHotCYCATsYHz3XC1z+c8RGlw7NF3/79DF//aMYMR2QYfjpv105lwxkhAzj84kYNS6a/tmx5dMDjDRo1vLX" +
                "FrTc1a8rd8uTp+vo33HlX60ZNGve8r9emTSyk2YRXw/ATpXJlvP8+XehwDwqYgtqQ/emhQ5QYctnOYbIKX2d5XUIaKY7xstD" +
                "RfL2TeIOBrdiMWTMdg/WTp4BIU7CTCVoo/+qrr7p06XL77bc3aNAAeOUArvwPjz8m0HucuOCjP2O1z+s4HaXePtAtjZ8Porp" +
                "167Zu3bpjx46zZ882L3Vk4dYA3ZTqP00XOtyBsBSZzCHANLZ+KssREqEqTvZSgK//snKlo7HeTlZVAaBZcxgrJMefGiMEXzd" +
                "GMkQ//fTTqlWrSpQoYXCptMsNHJhaANZYAfCl9cZh/4mDN/vpaPTx4kw4Lo47sKHErElfL/t6+fLlxg4UVWRKGFru/xRd6HB" +
                "HczLWn3hCiekLWt1gHaAx/mFW/PyzIybmjvHyJXPZWR8z1tk5TKqhQGRemF66i3Ghtm3bavgpj8fD/jp0oCVoP4mF48TAGi6" +
                "WDL5xYhLN4f5J/8ETPP16vX4/3w5/IejqoGuvudYUiZGenGIPXOl9yLlMFyLcMS6Dox8+wjLM0OFDlajS6TdX7lyUsu/vvxl" +
                "qEgWAfqFH/275947GeotWLZWem34q66xu8+MnEhRCEm3GCUhNnTKlW4/ucBMZC5eoQGcYtzYn9mGjPyQx/E+TGX+ajoH30hD" +
                "nyjxKde/e/YUX3Lj1KfIVvCddf1HELz1n6UKEO4np4OgYrwcNZvNdABd25Dt2s0f2OHHhkpQtK1j+pj/d8O1EV1zN4QNwy8/" +
                "C709qoDdCC4pBNH3adGCLSbx3AHY2Us/MAcTjnGUe2VcOGvn8cyhtUHCD8uM89AvPVboQ4U4M9dChQ+DuDz36iNLCd8ky7PD" +
                "o199WKZFq7FgA/xzYz60rnP1KsZbxiWzzw48cOSwjTM5GRkBHiycaOXJki9tkoCCoCdripZuBo5+xw+5a7sAiuOea8aZuobr" +
                "iqitHjBiBku/fz3WS8U5+LtAFCXexEqCm6tKli9JOAapcVoVSVkj8DGrdeOFwaONde9g/OswK6l5TT2n7dcqMhs8g1iln0FK" +
                "5vCHW8yB6V4wITphxQq8SDsqIh5Fe7FuQaC4d/wCykRNSjZsex7NzzG6h8DEFHj58OL4i9FPPbbpQ4A5hA0InmHTnrqlYr1r" +
                "tMkr5aeXPnELTPvHR5fXxpO3vvzkAAahi5UpK+y79/ofljl6gcdJHPN7riIoDi6+c2QkOHT5MSb+C8bABZcYxavL7NT8Oud3" +
                "IJBl52vHzmMf6IOrEc6V17d4NRmz4Oh7gxD7nXKYLBe4BWUiCVtvRYaN9llOAOZ98jJSEpEQYhCmtnwE3hkt1YB26yJDn238" +
                "awjBiQE/UrPnNeHhClgSGjp6JnsJhIA5ZH1p2XML4YDKE33uyh3mIi3uZ1hNdVr1acpA/jb8wCvdzh3jdVASJ9h3bM9pEIGn" +
                "YuBGlfDjjIyWmXe5CkizBuFiXW+BSHVtFt2zdyq1rLcWnh/Wg1l0YrEM6IqDDOQejJwxYGT9sNIuQzatOPL8UrSJr1sW3Xvi" +
                "Np3DY72LxRn4RYoRo7969XCdHXY3NOUsXCtzB3alJunfrpuALSak77mQnMG+/8w6dw2G0jx1JM9gNlB3m68W4SaUPbN2x3ZH" +
                "9SnQAysC6OcefhsyU9L1338uRO5eSeEngwYCLLSvbqArlpmkPKgwyAG0+HXKM0jEB4NLqdyGDfdhvNC+1TyIe5l4+kRTuUTS" +
                "L9cfExcVNnjwZX2p/fkhtnHUKnPtwP+Uqw3IptOzJIjd37XaPwXp3CXI05qWXlHaEZAZoF+ZCeSWWr9/PfH2r8PXQ10QqIaS" +
                "XgIwAh48edbmgcF8bcyHw4j/Fuy8c4nklWCRsBIBaiCgkUFW4tAKKOm7ihKY3N8P5oiWLr6rLwYSJPps3F8KSkq2DSr4xMWs" +
                "SWD6cGvjlFSElOT7i7cOsBtCvMUnYuHEjf/LhwwEdDQpVEVI/Z4v+A3APSimBHkOhOdIhTKHAYu/u0lnpRdAu93SllOdHvaB" +
                "ktmqCm8ZlibexbpwwEm3fznw99AWa7ILhBFK7oyNzZFCuABypYwDlMdIDfSJEzZozGytQV4jX1bLlLkbBfv1tVbce3XH+yWe" +
                "f1r6iDs5/XvkLThrd1ARzFaLmLW5VEueMrW7Smtz4ZGk2ZLTJyBEjIb95eBHQHzx4kIqHX0d7TDhHKHCOw90g+zhQS48wNw0" +
                "IiyV8K61fHzzkaUebMfqFcSYK96WraKGg3ALnjzGxDLht27Y5GdYxQ2oiWrFiBSa4/ozpFn0ieVPfMLYMRYsXc7TdDnT8SCf" +
                "6Y+2aGrVq4vyJQQPxgUpcJNx5V2ucb92+DSfvvPcuph/1G9RvfktzTvJ6Sl9UxqMHHAP0jJQzpMx+SzVED05KSpo5k03lglq" +
                "WC0YaAM8KnbtwB48Exg2nxMgYEXYRK/TwIQ7b0qhRI6VlmKHDhlJK7z59VNrNpggsAx2lA6xr+ucfjtXonIhRocBBDfc777y" +
                "Tb/Yw1sH8IgLF/OIAgyx/SfmDhziu/OMDBtCjRo15ESWpe009OPMAsAjWhYoUxqVSZUpXrc67ChU7unm4ctUqSj4ZPs+INmz" +
                "aiBPTB15+9RXk4ad5PZjjmpJkEPTcReNYPY8/4+PiPR7+hhqXX44as+stYhudSTpH4Q6WTJTiBBBpaPzEiTu270Aip2snMPD" +
                "vFdQOhlChKbLrFP4cGzVprLRSZdRodgrQXYIc0YDOnEykhaw50gR8BHOFr2qk4PlO+kqYAwc49Be/VBq4Z697lbF7iQQdSuG" +
                "lShJyJBIlT/jkpbe2bEGX8C4wb0pfsmxpwcKFDMuH3AW6rHo14usE3KXfLNsm4lZE2rFr5/adO378aUWHTh0ffvQRGijoXqo" +
                "ZID7GFwNzN59IJnEilydAxM+ANB/eb+Ge5PrrrnPgHNyy3U+vAs8MnYtwT9HObIkCgrObb3F11YmJiYMGDQK2kAcdA0B3NDs" +
                "x+hC4DAA+3vvgfUq5SSZ2idmyEtTg5BFBlILaPwy/RhsF4iFoIfM6q6RpyMgw8xawR0hqeyxXRTx8okthgxwdy4lwtvfvfXR" +
                "7iVIlCf0ETXjvgKbfNlRs0Kjh+IkTkNnQzt27Fnyx8N333yNRbfgzI4YOH/b00CHPPjfyycFPvfn2W18tXrR23Tq3ywodOXb" +
                "0rXfephNMox35cCpD7rx5aIRhi0s9fzBQzsjhQ0AR7XjnddnIawZncKjQijuDdC7CPWgkGaGbmt7E6NHMA01er169RYsWIQO" +
                "6B4BogL5//34Ta5d+358+jRKbyKOoMXiuJsyyTNmLHGl73IWHYyhACqR/FOY4WEdO+qWCKRGNIjJ1gwkjIvPr4mJpGk0988A" +
                "hnt5dXrMGnGizoOJxnQYTdejYActboHnz5/fqfV/1Gpdn14G2M0L07ZdWvLR7zx5zPvkY3Zto02a2fqtyWVVl4V7JwAJ5D2U" +
                "O/5Dw7wLQY2XZKyGBuUmZMmXwFtM0x6nG003nItwdzeAdA1CtlUOlG1aXJ0+eWbNmIaehXTt3tWvL/r2ghKZ/v/3+OwcwEvS" +
                "bDRxlLi7rWAtGfNXv8/q5GyAFDXNCrAfFnyPlqVGDXxGXheWTuLAVUxTe/YUGRqkrr74S7yLhm/7s93D/WnVq8/dqlBcvVfL" +
                "zuezcxhG3SgMGPkHzV1xikm9EzTDIdJx4THldcZz+FMEJonnqvUplz5mjz/3/A9wdjftSZUrRpXvv65U3X170TLf8aT8h/LC" +
                "v8joG/8v9OX++/Hg+hL3QujuDdG7BPYDBznFjjoLfgFWEVCu1ARR2aLaLy5erdnl1EmRNQ8JUHZC67fZW6DZxsusHfP0iwTp" +
                "o819/mRuTkpKQGFIw+08QGi+gdY6QYQzb9oVhnfU/Iid4dRD3a669xuOLmfr6a47E1E6VWOSzbmxQf/3GDSgMzTpIfDeFZPg" +
                "K64VGhT4THfs4xBC0dntgbmoqkMr2+BMDMMrt2rP7pZdfphPqDMhAUp9XgvvhReYhOEKaxvyiyyUkicsduFLTbkjOFp0rcDc" +
                "CA1eKLAnRMM2NpLFuV6s5j5OtmX5RnMfJnM+0LiNKGsNM7HAXBOW619TlF8kbXdfpIlfQ+IsyoDwoWGhZLUL/dLT3U2MaEPH" +
                "wiFExmK7SAwi6dNv27QoXLeLuQFWq9hV19olp2t59+xrf1ASJ4NnxlgEC0g3lL1ig7jX1qGN3696tU+e7aQp7T7d7mjZvRmI" +
                "S3OPYBKnDq5exUC1ElHnV6t9Qtu49u9MlGhuNzTOPWprZn/BAV6S78PCXXhLHO7pizwqdE3DH9zuWeEesmiooTu8mTo+FoDa" +
                "JU8ZIyxEUaMZGEH7ttdfKXeyuwgDxhhEq4Zr8rgCz5N179vCLpD2yZXP1MygSzo/TNmZuOkG8n3IDH1eV4ROeavpkjVo1HZm" +
                "YukuS8ps7T24I6Fu3b7viKt5EgifjdjM0KfmuG+rfOPqlMfMXLkAxMkLLf/xh1IsvUhfy694CBLMIpIO/KjERNaB3LGV/tuz" +
                "ZYvW+b3OEfylKi4ZjNuTzYkds377sHjk9VfIZoLMMdwMmDHOoXGDdL/I6Ks7gO+TECDlAM5rwUXGv7kgsJGKZSvNFYL1+wwa" +
                "O1q/v2Mm7lkCJWV0ZxtHLSTgPgbv9J4SZI0eOmFeEN39ImSkbnFa3bHWbksgfBQoW8Gl1+9NDh+ClN2MlSCqB9eICHaQUKVr" +
                "ksQGP/7FmjSktaPOWLQu//OKD6dMmTJr4yGOPPtD3QRLK+z3U/+VXX3nrnbfpEryg2bRx06ZnnxtpVmeBeyNoKQma+Y/s4Rg" +
                "7ftyTg5+ik2okLsrs2f5Mcx7+7X7Yk9FjvV7I8atWrXLCavWM0VmGu6NVHwR2+GypIh4YfcIVjHjKvEdvfTDAwuQVIwA4N2e" +
                "QqVilSuzzH0RTLiVjN+GmeIniJh1hfrEmQrwHiaYZZMhlskuLP6HUD+gCd+rMGmuULWQgMn3Sp5UV4M10V6s7bgekgOPcefN" +
                "g+8iYl9mGB+l+vcaJlO49exCmTflJ5HvnvXeb3twM3TiDlC1Hduppk6ZMtvWY/xw40P+Rh5CBXge1lelgj2j2MW166g7DrNm" +
                "y4gPNgIaPjYh4XKUZvGILiDqObnS7bs8MnU24B/QkLyXZ1cOUEK90VEEerydOJqNseq7NjyKT18OWT74YNt+VBUJYBBA72a/" +
                "V89hLgWlr53u6OHrXEnR8OXLkQLYTDrJooYCMQpC7evTswe8ScOAIaW+/MYMRi4BLK1Wkk/v69H6wX1/zBYhkdjQ5uZR8frx" +
                "2aOPXxoxYHQNt+vPPhx97pLBeTEUGBhO2/MlJLPUx6n5UHvqFwxmpGZTN3Jg1e7bOXbv8sTZ1oKBJM5x04znsliOWy0BTml0" +
                "7d1IG6ifvT/vgwKGDDRo1VNInWQElNR/x2+3DJzoxuqtRIza6Zn6hvSKHc5bTRGcT7hBgjLxeoHBBqguP6NRQiWiVnLlyPvT" +
                "IwyR0Gj0x6Odffxk2YjhMQZRII2h4mvAlJrp22L/9vhqZ4fwISyqt29ylNPoLFy7saOklgzUO7u5oH0w8+xRTW7w9vNUN3J8" +
                "f9cKPP61QAqay5cuhhPBO88Zbb+JPF6yas77yCvuaBE2cPMmYDPhELorXZmfM4MVawS/3YmQwxUBv9ENHqQdMpONpufLkHvH" +
                "sM+ZFNGjAUigWY4sYG9Ofz73wPDLgLkOuD4/j2tvY6XRLmzZtnLT8xTkjEs7ZhDtMcx351IKFGOuMFeIourE7dOoIgeHNt9+" +
                "67Y5Wtr45R66cDRs3enXcWDTA/8TGVcnIwIqLGB9xSAwL8PhFFCt6sTiZjaHS/bE+VHrG6xpdFCdK+gymyGhOG+5Av99g1+v" +
                "JmTvXt999p6wl0r/3szVOW4kCAhHCr1cVOnXhwH2gkc+yf1YUG2IbS9jaTIXV6vEcoRt5IhIKA9xDNYQO47dAT/SARNoB0QQ" +
                "AidxXvR702KvqXs115QRJRCSBnvpqrz69s+fMgYo1Hx6Cb5xDCqXnQIDcsmVLULMYvDGDTfBv6EzDPaA9qzBfDyRjyggODay" +
                "gWmvVqU3pi5YsLqe5YKhIoxXGRAUKFQSPhECMYR2iPCx4586bR1dpusb3WSrLbTvYCOf4AkwIyajLwswbb7yhtHIzpGlDGLy" +
                "SwEyswxaPwSgzpaONMVPkSQhWEgReW7b+havjJow3+WNFJcK4IYyKvIdLoIvLl6MJLgknI59/bvzECZ9//vknn3zy3nvvPTd" +
                "yJAlOxL/xFpMf0x6j++LdSbrDIGygI1HTLi5bllLY/1K8Oy+iD8FViIgKvRfjm97HHbFa8Mv1Ixynf//+jqxnB2HdnXzasR4" +
                "883CH8o74On0hODf4OlACcMC4Bd6leZi22jVfgfzFShS32TyWk5QIM/sPHiCZR2kU4oE+aSRo67JmTQInppRe9/VyxNb3pJg" +
                "KRK+ff+ZN3GDGIYcN9zhR8FHOjZs2FZDPhGmuiYoaK3ucsRQKLN7dpTMurfztV/pYfEu8ODkifLMsbgnfV15x5eTXps6YNVP" +
                "p3SrpEWW4tDJP3/fs3UudgQZG8xBwZWYNqC483+tZ+vUy3Pv8889TQpaELDH+GLjCJDog5uzVa1xOrIruyl+wQO48uakn4Nv" +
                "xOeGVg4PHw0R+zpQpUxxjiYT/TjOdabgH9ZYL8Mg8+fIq4WeMEqno3Xv3uCyQl8cZAYUKFXr11Vf/2uIyPEO79uymqRViaVB" +
                "mwGWI2Pcq6SdQGsRqrrlz9677H3xAaUkGD0GRMo546qV/i4Mx6mVGnAg/IOEYoeXxJwYYb70ka+HV/IfXY8KbEc1bMB+XsOv" +
                "KIwuZ7BkBLFMDvc4VdaZ9OB05QZhfOnox4WgKj59mb27bduyCb8mypfYtRF8tXtREr2FRSWJkMuAXr2moooZNeE5JtHnzZs4" +
                "T44FiAFe3yh6AQ4cPz5w9i4aU9h3aV6xcKVa0T+EVElo57NqEv/r3P3iiLDV7+sF+5uGOfoxKRD2z2CfjKRoMO4wSxeluthz" +
                "Zvvn6a7dxHOfPLZs//eyzTz//jESXtevWmfS169eVLFVS6ZkocVA83MAuVgvEb0pADkoktuSIDGpk8QwS3dW+fXslyorjwB2" +
                "tTq++XeSr2nVqYy8VvpGLJyMWeCqfeT2wm9i7bx+w69fWLxBgcG+fB+4/fJSNc7jwVOzkQIpU5pTXptJVzGToo+iAixFMS+h" +
                "SNpFAgqK+pBk/nUDvCYJzeiU8Ao5AWMrXnXDtmrXIRvMlLpgWS5RGPPxwGMJ4a6rCFmnMQYwsixiQVbmsKh4eTOFmOKm2OAU" +
                "6E3APmCAwxHJIjBHQsIiiFywwRC784otxE1lUxbJ/l66sMXQE5bffeYddoYaa3txs5apfke2ppwcrrcbu1qMHCYXKSDVS6ZB" +
                "5AB2Ip6EFTYfMZAMKGVg7hgjo9oF5G160fecOFAPzUcxVgHVkppPCRYvgE75c9JXS4xK2ipvxoUf3HsgTUuyAtmJQolhEhpS" +
                "jHF8qKPU8YsQIujR2rPQEAyYLVeaxiD7ikVkppHmWxQXWk6ey1EFUu3ZtJWZFAD2do9tQT3vtjdcfeuRh4jU07ypVuhQGJQg" +
                "2ESvKtAjdnizxsFCY04r4MwH3I2ItGICnF6kdpVVpflHZ0p/t2rc7dOQwp0sVYA3P0V52kR8gg4jJnE9vjrzmOtdRLfF+pXn" +
                "8/AULXnxptNKKBbuilQ4SlsGaTdbeaYhGPPuMEg4XsQnRipBc0fE+m/s5FikLFi4Er2PYb4qZotLW9kTPvcAiMoYFn7jO46J" +
                "6OPSA8ccEXZZdtoBeir79dh5GsGbpcFwml3l7vdxhOE3IvheE3oJXBK0hl1XuCfFZZGxUYiCJB9avXx+1SkXFpzkSNpl4f+U" +
                "qlevWq3tX2zZ0osQEzW/ZpUWoK3nybbe3cmQXCEqIooaWMpPoTMA9oLUxQfB1j8isoouIsQIHIJHO+z/Ec3ZHi6SI2MhGeWm" +
                "FwnixzjOuTuDGkeZhSgc0pT+z5czuD1vsVDr+oynV8ck0w/Lly7kk2H+U/lZr5u5ZEkqLQe+D/fpiaQn+ROtdU08J/0YxYG1" +
                "P9GBfXnXClBSSOjgf0arf2HaFgG64RkjZMOb89hsvEjdu3Njh+berXV24cCEl9ujBI0NQD1MhhJwIpfbSKy8r4TXgQbxQJfW" +
                "MNmrVinFJVKtWLaW1C4p7ckFK/PgzV+kECmmsiHVlnjB16lTHst0Injap5kzAPWhtUAKgzcfHiYD45ttvPfucqJY9qvRF7m4" +
                "AVFmM6JjBSFBB5l6MlTz0a9svDB0txcMoUas7bl/5669KxAbbFpdSHnn8UZQngwRIlSzJ5lwJEnYPkA0/fNoG86sli+mX+Fy" +
                "Dxo1q1q7lWIu7PhGOs+g5K/wIxEt/BtbpzxKlS5EURye4FxVoZEKbUvSyV0FZp+PM2glmmTJlKOXQoUN4QkQM2dy9pFi6o1T" +
                "1rr1Gwfoa3yX1ZhBf7pLyStoR7IZENUcUxzSavfHWmyQX0SS4aPFi2BIVAn374PbVIg2Kh+dHLOq/p9ML9xQQbGNkXQZLdPj" +
                "OOM3DHD2zVFrDBct1v56uhdRROKugCiUOlCdfXlQWv0cPGhxCTK844l5Kv/nWW5wMa9yNikMJY7a7XEiR6BeiqrJU7N1ERbh" +
                "uw3olWn9gHWUjGiTTRFjhxibQ/1wJd9zB/p6IiP3Tn1hOBgJCymxDBBGPn3vOdUu9cdMm+rOeSHomp03mUXziOPsPHKD8V8h" +
                "2k6A887FHeaXJ7cBeD+R4g3hUu+mfcz752NGyUIVLK7RsddvD4l3Zl/48B4m4nSY5QT1YhRc1s+i0wx1iALAOqdR8LWPdw0u" +
                "n2BhBdEuLWx3trhFGARHhHlJlRgCgu2iUoCfQcIwHDhsxnKQIVLrJTz3B2IplpGbxCZ/JxCBis+Gxpl0Bd95ZJ6adCLRNXwo" +
                "FH7COyJXvT/tASa+mS0lZk/AJgwcPdrRNzgfTpymtXUVlRhRIAvaENSu7VCC65dZblA59HIzENc05PhAD7Hvv86LHUSE6eef" +
                "995T2VoCvoz8HDRqEpylpU2LwSKeUbTu2K4uIu5vV4ohHjNxOOceOH4dyolQZaZdToNMOd/qG/QeZbfC+OOuzGabSuiSbNmj" +
                "UEMx47fp1lL9QkcKGbZwQ7lxlktOudJJElcCoSLGiNEzjPPXVMqRgu11GqhVooFto2mcLRakPlJUgv2ZUmHQmJSUSTCtXZT/" +
                "abUTzzaK5jGDjJ0ygxDXr1ipZpPTKHguIy+Ml2Lw7b4MaRwoPKATT0V0EtOlRx44dKT/NMXBjwUIsVYOLO0Lh9wb1BxaTxTu" +
                "Tn/UKIhHNnjNH6e0gED6VNvVZvHQJV0uMF5CFepeA+8rYV+m39//6wGdyeI3ZVWceu0MvcqdXzn9PZwLud9zFHldiLJeIhhG" +
                "ifrmlPbyZg86372DNXZysDfGGg/SlZPtg4R6+EcV3HD3n4vLlsBzraCHEVC5UDbBSQjnDK9fmMZSta9euXKq4OF9cutw9Rpx" +
                "pUbbhz4zAp019/bU9e/f+vuYPBV2HjGawj0CpAHG6C0KdmUCjYMh2T/dudOm773i7bSCMu5uSA7JYD2rWtOnYCePoBL4GcCP" +
                "Ivhd3BeVFB8StzXU3XG+/BUIO0ZxPebz1C2fJmpREVUh/YgUA6wleWTlSLMJ96miRpu419V54cRQ+P719rmgRGLTWq1fPEbh" +
                "nUMg8BTpdcIfyDm0weswYhR1DEobFfCrVUWJSoqkdmB89M/JZrh30h7DaSe9Afr/guEs3dohHrAWP3b59e7XLeFMCBgEfJFE" +
                "t2aNpnTBltv1LV9n1CrHwxHS1MUaaIliXKlN68JCnlahl6N68+dnLJEYqvJSoWg3ZriVWBhhtkNlubBRsw/oNdLVBA9mVEob" +
                "XoJQQ6XA8XbhoEe54soiT3i0gvAgs6RlRz0+fzou17tPgb1Dz+Fkz2VQBbIU7p9eTO09ufAuzD+OKVV76wYcsgxkCi4nT+lm" +
                "7DlFvXHWaNx1NIdxEENgyhU4X3NEGrCA4xpUF5QM+1RxURzVr19q23ZX2nhg0kHLe1baNEkYSUi/HP9zuIc+sUbMGPeedd9/" +
                "FY7/44gsEsjN5GPGyX/jZZ5919NgdkWs6mssqYWDpFQlPpleXr3AJXvr1t98oabx332PZl/MIpt98+y1KnDx1itI7QsDXb2r" +
                "WlEsSdNfjUKRjsvhC6Yg16ZxIcwrtytvvvoMyPP74486J4I7Pp99ChXjftyNzBlwyGYi+/PLL2qJ85EFY71Wlv/73wP2ObAh" +
                "UItKgww8Y+IQjHn769u/Xpds9xsAJo583bKwG3PFAvO702ROcLrijncxuTkd788K3mQ5NI/vvv/Ngp8QcnLLVufIKdapwR5W" +
                "VLXcxPWfWx7xXmmj8uHEDBgxQFtzjZPdkbCw9nf11hXP3oJ5kHz4MB2asyz/+LIJ7mux2u6peXXzLwq++oHu9/hjewy/z40J" +
                "69ZQyeIUXUmIWWSKgTk6DPq6i0qAOAuCmTOHu8fzzbG5ueH84MUr085WGjtlCEUIBGbVgq7dTNjE2bNgQtwQsh/RLliwpXNg" +
                "1sm/XoX2FipcqWQlB8ykWuHnbR/2GDbBsYhRin37OM3vq/1iRJWlepaOlAQNC22G1W+wJTgudLrinaGVws2bNChQqgLprDP9" +
                "1+jvp/NYWt/7y60rUJvbswGWz/5TgDsyR1O6wawCeYBG9/dbbDz3EO9PQx1C5DHrZp7N0CRtOBS0hGITCAxMoD4un6S8txYD" +
                "BKzVz9iy8l258dTwL0GCHdILNztilyrdo+aeS+HNUssL6zjvvoK6IOI6pEMqQKxdHwERRUdrQMsuywy8r3fpctpRNGpN1UMh" +
                "wMpimUY7yf/oJi91BvUz74YcfZs/uOmy64447MI88msw6AOV1NVR0Wu6S8pwuuoE4HZ6ye09e2MpHUpw2Zi1SrKhH7/mKWIe" +
                "sA5DmO3DwYCCM+2QWnS64Q1GwevVqfO21ep3/hhtuoD8RvvDaaznxl19cv8wP9H3Q0SGTGJHC/yJWTchhmC7D3cPR4eg5E6d" +
                "MwmOXfr3s1pbs5dnOhu5BiY0aM0tj6YFteTQJxOBicu6CeUpbyUZsKrfz6DWEXvf1wjjuyBScXyqteMP1NziW4wNelxVkPPg" +
                "gf/WaNWuuv/56FJjo4YcfDuqpKkAMqyGz0Q5lNL8oMy7BTyDR5aInCQqCUz9NEw9fjttDiou7NdxO9Pbbb5vtYK1atYKLwqA" +
                "eEMaOHas0P0Idwri6x73seZNNDyTRkU05dMKg15ReHaIa8bRxE8fjdaeDThfcjxw5EpQG++kn3hVK1PimJqhNtGunTp3c2tW" +
                "DL4zjIDkYREasmvQOAK7P/f9z9BxAiXEBL+OLgaupVpxw9zDyog134ZQQhaHCTw/u5k864fUsGd//2b9/x66d2E3C02LhcBs" +
                "2bHAsJwusk/GyZtNUAhFV2qOPPgorFyWBdFb+5hrAQb9x8y3NHUscD+HuVN3wG1yjVs2GspcUXvjS5NFE3wes7/uH7Zlvbdm" +
                "Czl9743XUIdE999xjFyyoJ6/0Z4nixZVI6h4xfjTm+0qGUHRjWCUp0c+8P+2DX39bNebllwoVLsQaqnTaFK9uenOz9Mr87+m" +
                "0wB2VQieQfbFsRNRAnF4QjRnDGjcMynSSMydblhIs6JxmZ0rgHp+B3b7+tIDzQ2YQI5NixYqaxUulhQdkM3AHO5k9m1XIKce" +
                "sWaBsNADcsYfNPN8ujzlHw193w/Vw9DXoqScd7RYKK2sVKlSglPUbNyjLMIbOP/vsM361EKahqJ9Zs2aVKsXr+Uq4I6HQYYs" +
                "ANsIx9ZZaWl1mR7NY6mmLxISh/yMPO+lAh18pH/iofCAMNkF9ZcBxjJMIrYbHXY62z0Eb4UNgf3/b7a2U9kGSRXRuCByipBv" +
                "M/ngODSM+LamHHHga5WwuC96hxc0kOi1wDwonQDUli1RjJNrfYLInUyjIx0Q0ScLVxUuXOJYFQQi8QmrHPjeCSjYxgv37n3/" +
                "4cV4P8XWs88Xq7cMGtQA9XVq6lMX3NHCndrWM8k0LhRQGD4EyET0HIYjnf7EA5seUB9HlF8yfT49CVBkY91P3KFikkKOxaxQ" +
                "yjuVbas36tWZTCHWbypexiA/oh8PdlJa+2pzTW3Cemk/fh3QiuPwFPSTdw9GTFqiS0RVZ+JFEFPK6G3mIphqAsE5TUkdMtZU" +
                "26lRSG0hBPXPRZC6L+gxvWSRSth3i9SC1zJlHpwvuNgHWb7311ieffIL6CmrVB+ruyy++RKUQ0OnP1X/8jj/TW3wGzlKBK4l" +
                "ZsjCw5sgrDK+aPHUq9i7Ylcu78fXQuegrdiOMItkUEECMfolXDFwRPJ0ymJJAr0ozb7oRevdY4uIySxEIMf6yZE3y6O3hy5b" +
                "xVDIcuIZwF9HQoUNpkMAXJSUm/rCCw3g4orCjuWNAa9zHj+fdAi+OGoWrNBeiP+fOn8c5hWTYdUctR8cvUbJa/MILrBZjYo+" +
                "0TOkVDE22c+cuhQUyaSM6h9um0heVwYBGKS1vv83BGqLXc0/3bnd36UyCDdsG6wUHu1ZRh3AbCCuj0BdnBp0JuEPb5dam5hx" +
                "BLfPgErggEbZruHGU0hFmDMhwEqOdF2Du62DBUi950C+2SuBGHMiP7ZgpOlyZXWbItbdIJKOIKsgQuGM9lQhYZJ+MOt7T/fe" +
                "zcnrYMDaHxNOo+fPly4eipoeqoLAJIy4TzZ8/v0CBAngIzRMmTpqE9GMpyTDfzZaNLaUdqVuHF0oP0p9Vq3OE5IAm3ILNgUr" +
                "YLf3OFQ/D4EqQqY5PaLKq4t3SmD/AW86r43gii0qmh1PKE4MGUgrxlyHiI23mLB7nGdm8YzXVpaZ7IsqG6sL1Qt+aGXQm4B7" +
                "UDN7W7uHEnE+YzK77CQcJIvMRVRJzi4hwN8CFLIEVHOPNFJ6jlaxbYc0FW2/MXeDr33z3LcoA+1gsr6QWWFq0XYf2KgPcna7" +
                "myJWzV+/7KPM0WZhEi0LC2SlDc+7cufnrELJLqVdffRVvt19qE2oG5wQvzIKwJdwWPwhk20VF+O337NLDmFKCrhJXprt3c5B" +
                "7on379nXRQKeSf/HVl2UvLqv0fCBF646PUyqQOyZj75Xo0FClDo8N7rfDimbNurXE9ZXYivFvjDdfgfxm0LYbFzUJMQ/KzdC" +
                "3ZgadCbijdlCVQash7T7gyGI7as3sToLjWQyOOFIBZ8FdiVoXt9zXp7cCB9WsnSUKy2oXaFv45ZeO8MWgXjoNkWcAdxQgI9x" +
                "dieD7/Q/LDx46hEXTGNm5gk64Q1aO/eItA33A0XzUfm84ziCEAItAPPsWlTn9i2NGo58TNW7SuJIICaiEzZs3f798+Y8/rXh" +
                "L9ub2E0ekLW7lkUrJItFsMdaF6V7Tprygi7fjXXYBwJLsFJDZlcY1I4hXbK3EUy8sRaEpe/+vD7KZfeVEiBsFSyG7Pv16/wM" +
                "WCkNfmRl0JuAe1A1p6s7UoDkBa6FPjRODb0zPHfHShjriri+Gsn4Ru30EGu01BavWRA+Jo0NU2cHDh1x7BI1y1KnSOkFm5/L" +
                "yiM1JGf7athXvjaiFxDl+6RKU69CLN2jYQGk5oU3buyjlGTFCjpU5nEd5rrzySrs5US0hNRNC6BV015AhQ+hRH82cgU/+fN5" +
                "coIe9aSdmaSFrWDaxpCcVQnRR2YtmfPQRbnT05tSvvvrKLoyTVtGJnEjBhBWFgfgERRB9F8S2Tp3ZFRR2sXBVez3g01gKgE0" +
                "U0Rbxpu9Na5Rhsw9l9Adhc6p/SYEzA/eMEH3eokWL0EL0W658ORMHb+yEcbAdt+mSCpcMGz4MGRysY+tt3dM+nL5A1N7AYqz" +
                "IG8A6gdiR9guX11NJmhnb+72yyTAc7mghpEN4Jbqj9Z10oxIWjtdBLKZRwiOcLC6B23LhwoUoAwBkPiHd8gihS+zfv1+ZGDv" +
                "aRqNDJ7b7zYLQsGlHM79MzZlNxPo/l2mro+dL8GZlvxf4tv+0T/ALxoTRD3tWfFozljN3Tkpcs5YNmw1wHYnFgPpR4kSo30P" +
                "9TYb04N6nDw8Ldhkyhc4VuGPUpi/85BPeBGQYEuzGbNrw58YUzXVANGnjrLJlif6dMnXK7r28eMmBvjQLAfjgnQuL885xp2W" +
                "Olouo/0SEO85N8/To2YMaksRZLLNT8yN9v+yg5eL52HgGPblly5YI1Ipi2O9Nr3VNUSl/zdpsrUXfiM+H85wEMXBIkMlfSFH" +
                "t3t68OS9UEW3fxvJVc9lPE7QAjVKZ94aXByl4CMxXIZaA0Rw6yhIXf66eI63fuAF2IvGW4x2a6nA/TCvM2HBv2YKXvUIK8+/" +
                "pHIJ7UFf3H7LvQckoiZMWLVt88tmnNCczyrnde3YvXbKkYwfmakoaGyc//fLzjyvY7aipO8NooSk7fORIAKaa6WCd61eU7uN" +
                "lfZdt7tOBe5yo2326L9HMj+76fO5cpVe1suXk5cYtW7cosQnjwuhuTHT1VVevX78en2NkldCSWDyV88n3z1+wgG4nlDvaBTE" +
                "A7RdFXrx29msf8TRFpkuCyFq1ePPrAw9wJ1n4JduxORaqUAbzZ8CKlYfWAWFjHihWhDRwKBgUVK9xOcROSpk4eZKjFwRItpk" +
                "gf/Z7mBk8OmdEuN97770o1fkJ96BV0ajQ1q3Zv5yCbKMVEUz2uXARzPqvvZ6jeLoegkSc8IpnfkST3LhxIx57/OqzWxSb2dA" +
                "S5tc+TDq/QkrlGNM/aelatXknB0xHqDzwFdWhQweaPFxaiedzSkJEQUfkiJUOFi+T9X5Nt7QBVpYny7I/LHnwwA9nfMTvsuQ" +
                "WSDKIo+TX7BOs3W/tvhv5/HOFZAFYv9e1RUuxFkOgqsIvqxOkgWh221a0VaDJU6YMEUkd9aD0PAoaLb9UQpfO7C8IemHqBhu" +
                "kITqLZZTN3U194q7u3Vn1ft7K7sAZTuBhgmj79u3GzwyI61TH0zPUrPnNR44d/Xv/P4XEH3ScWCAyCLSC7O8D7GgXgvIJ4R7" +
                "UI3XR4sXwELtJQprHHBUrV4LNeitZSIdr9t69e1MKFJQ+vSP7ZYny5YhNQa067KWIKG/+fDNnuyEEoapChbjVEmDHYARKKhm" +
                "EZmzoVtrfixl5wGVRAzSe2IptU2AjUdwtTobpg4/pajFvx5o3agzo/+uvv6iecSNJiVjcJaJRV1kT4hsb3EiJiE4M4NapU8c" +
                "R7bB5b+ObmsCdlr0T0qe5u6kofH56g/Cp0bkI9xRr3Qd1+u133z024PHmtzSvVr3aZdWrEUpuv/OOp54eDE3I0q+XwZcLgiC" +
                "YuRrqF3vMwKWc48rrNjkYfyVQno2YiHCnjFNeZ08pRLWvqKPEFFYJ86MUxBJDfqUXU49qX/U7d+1qpX2kJWZNemkMx+tCOdN" +
                "wWZofWm7uPpo5QwmDt+FuhCvIA1x8j4c3HKZFPKR8JVGzjf97fHWKjkKO5gCtXLkSATSJSpYsuWDBAqSjeFs2i6iGtVWvp6z" +
                "EM/zm228VhjiPypWT7ZZvbFDfK9FWKL13n95vv8ebb0IKZqoU5d8ioUoy2F4ZpHMO7mY8Bdwp1ehnDP2yauW8hQtGPjcS2GL" +
                "SPqv8oqxkIVUkHOguwKMCIioch7vjEgZQRy+XGMEgvcOnbZt69bnPkdBiHu1rDhZg8F9pWvGgeBYh4YTX9MWC3JFvxCAAevr" +
                "pp/GlsJ8BsIi2bttGb0EedGlz+EVogT8pqoF27dp16tQJBut+zTWNbEO/ZhaRK3cuuG8ABa0Qa4sXLS5evDiyVatW7ccftfG" +
                "C1UaOlqygFaAuRylYWqJqMUFqe8m8n15tOq3SdqamJo1sg964aRPPtULa6F/SuQL3oJFT0xLGVjB7VJPR+oFQZalcQRg8Lh0" +
                "9ws1m2iaox4qIL3J7l5WzRMkSSoflCEc50l2si+BOeKW78hfI7/P5YgTZq8QggjoM5m1eGdxRBv1WtlBhOxbH/bqhI1xbAyX" +
                "yqxFsRr80Bh7AlQwCN9S/UUWCi9KL+YbgI5IuwQufzf6pwJ06321eV7N2LfgXIHrrnbd57Vbo6np1t+3YjvQQXoviKT01R81" +
                "Typ59e5VeH0QKPHhSPeTMnWvGrJlY0wip2BgRPt26jXM3mgWTM1N8P4fgHpGSrS3eO3bzHjOlDYwwiAN2+EUK8oCJAr4gPBC" +
                "JaV/ikknH64qXLAGYRoQ7EuNku3GWpEQzACmxD4FnrM0ShJpLI+5WlF5pp+HK9mV+jGeh9MogOL0j26BMEJ6qVdk0BUSS8df" +
                "ffUMZ+j/MC2oonikPpWBrCxvriywEPg23H5A3TEVRyvU38o4TR/b1YlFWyaocIVIJFu9/4H6amyIPJrMG7qgrwD1nntwesXN" +
                "Ef0N+PME4kZwiLp9cmUfWTVUkmygMWTHiowXPyVxH2Oc63IFX+uxvRRzk9rAUyXZlmVYksseEZMsrlTRZurVnLnEtg2mFtUf" +
                "IQXydcezh2dva9evoRu6HJFAJ+CCKKC14QPUubZgG7inihNqW2Q4cPMiWVVoHRWCi5kdkGxDW1PDV5vMp5bvl3zt6Oou3O6J" +
                "7VQI+MxL6BVjGGysIFpSGihUvNnnqFKyeEqVI5AfAHeyASy4v4s3X4mMCZUB+JV+NDkAipbu5jLIlxB05dhSb/TB4mgMFM+3" +
                "4888/81syNczBuQ53MGZUH+rUL/oHnKTWkW5vj2XhbbCe3vZkm4B107s2bNyotAFCeojH9kIzpDzQj3dF8CwiNjaVkeuGp/w" +
                "qxpsvPxtCuoFZAm472kB/ddxYe+v+/X0f/Pb778pVKI8UmpHDiw7Cb9hlw+cTstkkWPzWoxLwWJ+EKgAicQudFyjIe4ipF9n" +
                "ThlGjX/xt9WrbGOHymjWMzQKeaR6LtxQuUthwH2V9NS88yQzq0OHDEyaxCaBxAYsM8CVvym+YPZ7z8cds2MM7DzKPznW4B/X" +
                "MyY2ZEcuWGKgXI+dhJCWJ2etxdzakaHkdMoyTvgBjE/LglsPHmCOalrBRnnrIYJ0gpn8dO3XEq7mHxLv2PIAxFxtwp+x+Dn5" +
                "GvD0kiuCCLxbWuYpdMIAaNmj4ww/s7M7Q72v+uP6G63GVJIFq1at5xZORXTYlq2yOzIOPidMOsOF9+7nqkBMSNqSFUqVLGT1" +
                "vtmzZXn/d1S0aeu+DDy6t4K4PELVu3Xr1aleTI1PtYLKYzheVjWP0THR7ZFCWMorYOczmlLarOXiYNxnGp93qbrCO56AGMtJ" +
                "wGadzHe5oMBBMA1hc1oZifphnsVqAdY6O5mdoaQAXTzipWsPrWKOS/lTVTYxlCSFXntzzFy6AhQKXKj7OE8fNvGUzq9KUFrK" +
                "VL5Xlg/7cvLm7LIuCLi5fbrw4zTMUEDJF2rhhQ6tWrNdXVqQ+Ux5KvE7W2g4cOnj48GH0eUdbGWAUYr4geIJOkKhYsWKfius" +
                "vvM4VV1LcUY7o0KFDAwcOzJqV18iI8ufPP2zYMGIHuEqUW2T3WMvBraNld5TK0THVMJe9pcWtLcRLc3it4kOIqXkszhXaPP+" +
                "CznW4O9oR3JdisksjL9djGCdQsmMNEEc7QYABVk4K60GNLQJfRqaqJGJB/YeZX/acOXxaCfPTCo5fYFx128zvxdGj8xd092p" +
                "4vJ6HHnl4xy42i3e0tACgm5KnWPhzdIxBM/rbz+/3cH+TzdFhuFmSkY0Bds7yFS758ecVyGb6hvvGgDvKoRqRZ+XKlW3auBv" +
                "eiSpWrjRzFi+N8Sekhfs/YsdmOpWjzSQxc6XGuvb66zxpY9rYFcuikU+GQS2UohL+Pf0H4H711ex5hmjZNxynae8/+/iPGI9" +
                "ptnjR9TrW3NSAPnjylWXuVVpZHhHuaBg64rTl0zffsxtHGhO8NEUTy8cF8+ZziigQSZCl/+ikqd5+SlS/Qf0vF7P9LUh216W" +
                "i3P5NOcauQXhnHe+942pB8eyyseJPulnW7Nl63tuzS9cumAYkiDdtiDGcU+MSvsVRS6g0nHPoj2PJDgnNYkuHOglq0BN9OH1" +
                "6paquJkcJk8aT2ZOMeG+lWbsyE3SBO/ZoG58FDsofZkTgfo4069atru1qatv8azrX4R4QlnPZZRJeT7s7xbY0UJEiRRytJUC" +
                "r2PeeWmWZ9mBYpLPM5HY2eIrzenbt2Y09oNUurwYRiG5/6WV2uEBcH4+KlTimdH5xuYsnT5rkNruIXkBbIEwACy9/QPw20tW" +
                "7O7HK3MgndtkwQVQW0wUfBeP06F2FYB+8apvs8gh8O2bQoS+2ahjFPrB//9NPP02yTYwsl+JFZS/iVdXvJcwJmBHNChztQ45" +
                "ejfkxvArb3B2fgIdA5vnzT1bjhtfAv6H/ANwxVa0tQbCUtrnbsH5DqVKlbryRLTTcNtNMKFOInlmgUMFY2VMSkbsjkbl7Qjy" +
                "Hy9QePV3nE8LVHnqIN/ZDdMZaAZ2QFIsyO7qXokVNCii9ZjaAe+453jLCAUplT6ABtH0g0Vzyy7SeNadSWrMFLKC5eMQOBrL" +
                "THUvL+f333yu9zERyXf369R1jTCpjyOXVqzvYvKulPkqn2Q6qzq5bU6U+PSYEtY1aZtF/AO5QojvaJxE8phsKCCM0S1Hptdb" +
                "JEj0KcRDCgW7aBpwVYCIA5cjFmxteGcvWUYD7FVdeQSlT33hN6Zmi0ltA0I0BL0PB9FFuKKBduftk4qtEdMGewJDihf/Jw4t" +
                "0uYcefqinbET6YiEbALOYpAeT9AoQsHQGjqXifOqpp5TeU0Injz36qKMtIjGG3C3+sxA3OG++vHe2ab3/wAFohNIrp7kLTZ9" +
                "ekU6BAuc43FO0k5MUkSzbtG/LtSAzUnD9gLagPE5TnQI5ejeTPeCGHOBPfr3DEs3/+Ty2d4dbeji92bp9mxK4U04a9004SMf" +
                "i5SBACl8RPlIhHXlmivvpoUOGdsf2OcuSAocpoTlHAYi739iAGTAcL190ESvCA9rHjikVKjOkSu3EgF6gqHH55R6fO6GnBy5" +
                "YsIATa9bg90rKK6+84uhlkxvk1UT/e+D+9OAOpvA/2c1kxKfMosA5DnebUFNoD5BpDLtVMoFke8e99/Vi4KbvpxJtg2ae9uH" +
                "00heVGfPSS9jNBI0ynezb9zca20PIwzigDRzs5d7UN4elGMItKcLwWDbQHQyqzHjtKj5OHFDGyFQBHdIrq7mY/xHd/8D96za" +
                "spxtvu43XklauZMc4KIzhLOaNOAG4ccIlDIh9m2QwylDAdL+46eN6i4/zCL9fs37drxIQDhVCtGff3tx583jClLyYACAbdl0" +
                "FLzRh5myRo12mpMfdTTsRtohho5EGSThYJcYh0MF98im7eapzRR1eEtZ+EGbMcBcpoeROj4AtdIAUEfEDwtqxGZQEhrXr111" +
                "dr67RoKO0MWINhgVLI0GB7hLjGVCuvLl79eL+XLWqG7f6mLgDCnk13m5ebQgGzL/9zi5vvRJsR8mWPEpE5GTu2FoviXgWoAo" +
                "VL4V/B7/e827Xaoy4WVbaOY9zoendzxY5OqpWeJPYcAcTZQ2gnl0Rsbmixw1E06Ejr7YOETtHP5uOMTSvvvpqNORxmjMEXgE" +
                "tPDhay5Ek0z4lM04EpXnnvXf9eikAhDKUKFVy2PDh5lHfLf++s2wmMtS7d2+z28uxBs/0zomgDO3VmzuMHx5elWrfsYOjIy1" +
                "zhfhi4IngenH7XL7CJSTX0Z8/r2Sfz6g0uz7d6YfU5OLFrIILRhLq/g1F4R6JZPcQVXfda+qhOSMeprXAQZcsW1q23MXEDMd" +
                "P0k6M42Jz5MjhsA8PFt/9sk8HoSQdPckOfbVFKa5kkbq69Pnnn1evxg40mTzsXG7Tls3mqqNdHV17/XXffv/d0q+XKe2zhSj" +
                "IGsY09jnffvtt+/bt3acplT179r59+65bt85kCMi02C2ERZQOs3WenorFNVYe6O2UeCn8zMiYc28f3s+ltFKySdObDh05TP1" +
                "NhdUq4O4RYzjYrnENCIXWy7+gKNwjE2xa4FU9Vm98Dj/QSNTk9a67tonsWvpIBBX3LmnyfX+z+J4rV05j7UOJMFABz4ZIAxi" +
                "5bxfdi4EmjTOI9sMknHvwEHf/Bx7iWKqSvHnYTt1hi5f36eSVseyujCVtBrs8XX7N7VjTuPmW5u7z2ZFlIk0lSVIyeVBCI9i" +
                "geIuXLFEy+rkmEujDTlBZ5gM//fIzbN8BZTyf9bZhbpVQmXhO+f+0F7H/Hgk2HK1EPyHcqSEvq+YuhAEfhYsW8YjhGqX0k5j" +
                "3rpNUyUzpeXLnYXAIgNC0ZuZqFK+OOFyHabgSWwPe960V/OiQhvmhtzgSR4ky0DybeDydQMi2vi2VkH/Dhg2KvarzRtvvf1j" +
                "eus1deJ0Ss5wHH3xw8+bUAcRsB6PzK666UhnrN6XoRke7waIPNNqqx58YgKeVKXvRoiWLERIQTjgMxE1lesXUjOT79Mr8Lyk" +
                "K90gUcN1dY451Qrib6eAzI59FuHQEQYBES3kczfb8IuaiG3zzDW/U0C9kcjHlOCRRtGzphronqt+wwVeL2FMxyUtKe6LD7lW" +
                "ryO6Mli7lz58fg0CFS9mvvMkTTshftmxZpVcxQfQiRCEGJSUl9e/f35hDEu3as1vpjeFg21u3slyOzgmsIyh07jy56bxBo4b" +
                "rN26gP//auhUZIKkbuKOH4EbsCwsta2ZQFO6RSOBuTFP8aRcmI8LdLxzuhxU/0u+8BfNhIwVw08m3EhUVO+5i9VpPieLF0ag" +
                "2Ox89enTevDzWK/GObe8idbCfUOxpHZHFsdHH7ipYgvjqK5bgid58hwP9WZ/lCksgiCiUYelS7kU333yzA12n9ojtyFhxq3Y" +
                "uyUVKyNKvXz96y01Nb1KatRPcS5Yu5eiYuAna7esvK1dCkkH3BiVmTYrT2whtJoK6hcPr3XvYY5Rd1MyiKNwjU0AbL/Tr21f" +
                "pRSJI3hERryTqIserYR/zvC+Bg05KhBzKUEVWgqFABBqAADhfd0Rbcs111xhMNL+l+U8rf3ERF3RE6R1Y/Tu7vW/YiINJAbj" +
                "h8AVK6DevyMdb/9pqpB10CXOLeQLuuvhi5sr79u3jPMZ4xhpwFi5c2Fzcfxsi1MJNA51P/+hDytNB1kq5M4tdg6MNKECNmjT" +
                "OmTsXjDJwhNQkJr6UDV9xOigK98hkWnrQwIFK5OaEtPblNtzpN2u2rFDjFC1ebMKkiYePHvnkc3bAwgO0dAbsbkYEm3hZc6W" +
                "jUJHCJI4bWah4yRKYWYKgl0Db0y+kixXYwBFir6sJipQDB9i1LxExZmQ2H2Vy2rinPMvFqAuLO8iP96IzGAsZolWrf7PBSgD" +
                "NmYddazhpbdyHivtOJU5pho1w1aCIAB5eh6Ym6eotLV0/fqeDonCPTAZqxNWUnmJG9DkD7o7A80Tvvv8e/dL8jO6l/NRPYD8" +
                "DJ2dr1qxhTMS4lltoYKJuPbtjpRN0TMeYtHFG2QoXKexoKDhhWDe26S++yC5I4YR6/352UhneN3COpwHTBQsWpPxwq41LUoT" +
                "U9VTsHMcH4tuxujT74zmU3k2gjGUKJZNUOMOB5GYovALNEeNlEei5554z35jpFIV7umQAoUSLnN6EFXD3aQ8cQENitiS68Zn" +
                "n2GOoR4eT3rSRHac0aMC7qn1xYlsmyNj7N4sQjvicSQ7yWwOiCQVGQU+JYmeS+GlCwQwQDVEfOXaUEVmyRAkloQTot/6NbKZ" +
                "i5ByDePOQoFZKfvYZB/5t3bo1cuLX5OF3yYRByQqR21c92he7riVgvce9PSmR4+xZK75UCYlJieF1aDoAjN727t1rXp3pFIX" +
                "78QiAe27UC9zGgtqIzAmIVyK+lxAnSiSTjHmZ/YEBHLhaumwZAEsJd3QVz/FxWUTMRRtj15x5O07oksfj6h+lO6ResumoYBo" +
                "7U2+QrVVlJTjHP/+wC4Ognpsash+CoQyzZDOkBPRePiM7lSl7ERdeNuliFg4H4s1EbZ8kkadQzmXffsMQj4tt277djytWbNn" +
                "2F6rCxrc592kRiObBjmwXTC1lplIU7scj08xKbHpDmso+sKIE/3hEcIPqcDTjyQr2sdaUbtxE2bgJG0kROSDqJAsZFBqBavb" +
                "s2ZTngQfY5W/aAloUcJWncAoLj5PLf/yBzps04Yi2wRMtyDva23i7du0cPXMA4jE49P5fH64HsZuAGIMQtiTNK2HegOyD/Th" +
                "YSK48uS+tVLHvQ/2J019z/bVFixWFYUXI/gEDd3AEVELmmoXZFIX7CciFu9eD1fL04I4GA2/r0KkjAkQiwGqOXDmppaFpRos" +
                "SVZTIU9T8sOWi80ceecTRhvvm1YB7CRFOSApHStoCakJBHQdrlnQC55ilhR/v3Lkz4oBgCFcpf0IC82xHWHuKOOsE1t94/Q2" +
                "lByXMCmgOim+J06hF13WsSLqGfHr3IFaXjgN3J307on9PUbifmKgBOnXhnXK2X5RwrBtAI142Udt2bR3N/LIkJaIzXHn1VWh" +
                "UBoHfx57ptT3ZuLHjHDOt1G8niV9ZQQWPg1q6umHTRqUDT8OsZcXPHLUcOnXcGxCyb8QbAet33nlH6ZGEuXsKX5orTusR0BN" +
                "fSn/u3L3L0UEfUC108va77+LTkP+G+jcipC4qyiNBNk0duljX/aRu3bpO2g6f6RSF+4kJ+KD2IAE6HOupzSagJ/H0/WkfUOZ" +
                "ql1e/7obrqb0dbYxgeNirY8c6VvhY6CVxadLUyQxqR7ZcyAmCXa5YwS4DXFuadNDgcHSqhynz3HnzOJs4JHJ4pZMl+D37eAp" +
                "o7g1/iPGECkEF50Sfzf2cv90XQz3Tz9umGesfzuAYT8u++RqwRneF1ySSpkg2m/La1OkzPhrx7DMNGjWk/hwCcXPO6Nffvn7" +
                "9erwxyt3PJoHtffop69HT4+5+OAKQ9gOvGjdhPECwZBlH1VIiuvi1+STU8PCDztm8Ht7HKfYwz4x4httcQhjgxrzwPXbcoCN" +
                "BQXaOnDmAVLOz2tESfKMmjfAQJNo3GuhDdqIup3R0kFlzeNpAiMyaPZtX7+nufR/bOUIbgy/CRx1LSUa88qyy3OaSBBFKr96" +
                "MxD/8WflqoSjczyYZNKh0dnu4XEoaj1Ok/SDDKInPQU24Unb0sBveWD8cpqJpsY2NsA4tHiKR9OrVC1efHjqE/oQ5O8yznHS" +
                "4O6Wv37CBMsNoB4gx4C53SXm6tG8/22ZiAhoCKUxM8VJHcEzIfkU2uMB1FI8/Iq5ALnL0JAHTDzp57/33KTEhifuDEiN7Xhf" +
                "zuLdHhLtfC/SUf/tOjg4bMlnPdIrC/cQEfFBjFClSROmFw4gtB7GEzitWqfzp56zGbtSkMfHmJk1votsfG/A4YAReCJ7t6BD" +
                "h4P38cIkHX/EStu4y9o/AIkoS1Dg2vyhez/vupczfirsboNnRGvTvf+BF06bNWaZP821piZ8j+d+fPo0xK5MKCFowg4E1hKO" +
                "Xh82iEklrlEgSziOPPTpzzmw6Jkye1E/cFCdlTfJHqjFTjTESXcdh6cvtolG4n00ynO+bb1iXnKC1afZhmtAvUk2+/PngTWn" +
                "s+HEIS4SIXDVr1wTi/cIpYVlF1OWerspSS8fEMuJBUO8kW5F2AO4Q/k2ktM47KPNLXErRsbBh9rhnzx48B0IRrvI+EhGcAvK" +
                "odRvWJwpGY8TSwcgqmIcQUbdRsqjkl9lnztxsRLB46RJTZkP4HEgsIVjHYyH4TZ3KsU9SrG85TYiPwv3EhOEVzfD4wCeoeWJ" +
                "1WBi7Cd2GjGNhBjignNjNiW3Rm7awhS0JwcrydH5x+XJ48qAnn1SQDcRldpxsqaaUm5o1BQRSRC0Y1PzPAMIRho3tz53EX4U" +
                "4RkqFCwq/atUqftpNPM7AtCYgIg2uGhet8JlB7BYbyQ1fhzm7w9Nu3tBNAkxclgRccrS3BSVSWclSJStUvBTRlwy4Q2tJw51" +
                "uuUUMdZKPsd2E+Zwo3M8OAQ1grkFpjCyJLiLDuTsaMkFCsSqR2uE4O3uO7GwgqeV1ZSQikQRKX+SutkJnp7TNAp6GlMceewx" +
                "5UB6UBMUD827blrddQ4EThENtTegnjmbw+/btw+1Q+eGBjvYmqURnCqAbRI4a8yLyIKQrjDpx6bCYHH/w4TQS3kiYeemVl58" +
                "c/NRVddnPYTjKQ+COr3M/xLUSSndykikUhXuGCPJMUFDS8raW2JEU0n7mHLM6OiG4Y72p492d3O1OnlTEK5kFQtdh3Fzu3rf" +
                "XJ5KMwQROkH/AgAFwvAGIGEHF0V6WkM7RQLSo41hy/A8/sIqmvjj6IgEGNxI9++yz4NNUctdtqlYOEv2sTZGLlSiuZHUJS0V" +
                "0vmETb+guUIhty9KQREc0A2D4/J7T5fmXVq6EEjoBHpOc04n1YBTuJ0tUX0ePHKV2IijGJrhBRMIFGzBvCKZEJA0TywRqbXN" +
                "Zvw51hs1HX3/L+5uI2rZvp0TggSozDoEjtblVvWuvgedAQwhLDcMsI58E9QgAS0aIK4iLBmeoP6z48eZb3NiRENPxIjbTl6L" +
                "SfBTPZx9MUkJ8GsauDX8y1l8YPYrmqQ2bNK5br+7V9epCHE/FdFqU48DIQNIQPeSXX7gvccXqrSpp6zuTKQr3kyNIBTAeRHM" +
                "CIqHc3XKmnjsvb0tVssc+aw4OhQd/LJzocbUfBkOPPPYoLn3z3bdg/HgUP1bEfcN0lZjoTHltKoG6TVv2Q21HokyPvlq8SIm" +
                "vDvMcM9NAaf3xriXztA+n4xY4VoiRmMx+Hb4TLjTatmtLPbPT3Z3uvKs11tSy5chuquI4cMf0fcwYdhnraI5+urEejML9FAi" +
                "rj1OnsEs9zCxtvbJPmD24PiPYo3Lnyd23fz9gCAuuhBj0ASKEHGMfQxIkVckGkV17duPqCy+wMSajLdZP40OsFqmhH8QlQy1" +
                "ua/nYE49PmjRp+vTpc+bMmTlz5rRp06ZOnfrEoIG3tmxRpWoVNliQXsoFRgnlaYS/LFmywCMI0f19eYEJdJn45fSJeTOK54/" +
                "zQ41TsgyrngxxN/B6YCaAcSki3E3HfvBBju2TIs4PA2HrAKeJonA/OYKoQBLzEYnvRe0abkhj/wlOD0C89c7bCDzvItWjDok" +
                "VF2IT8KO0GlvpRU3QgMcHIBFPJlRBHoDYgERz43HIlVh0gXlKnXZu0LFjx6BWBcIjLO7i3iWiVEURtYk+nzt31erf1q5fRxN" +
                "TGmQKFi6kdDQy0/nD4e6TAQRwXycObYByM9MIqe1MpyjcT47QJC4Kn3hCyQJ7xKkYDsghxPMKFipIt5CsosRnLy9JijT85Vf" +
                "sigiRuoAt8G/8OUX2c4DGT5yQS3b1M3ndaA5mZgko+8KcXNMEw6v9deEWM882k4Fs2bMN1oGLHTGSySZCF56DZ9KfTz7FLgH" +
                "XrOddIyGEvgq4R8S6B24reXWWX9qqFS9LBTXQ8d4o3M9RStGqPbgj9Ubaw4qDmZkg3uv1FC1WtHBRXpf9Z/9+kgQAPqXDyxw" +
                "4dDBv/nxAD8ktHGZMlldJxpgyORX0hDZ6KftoNyQzyzhZ5/dbO/zBR82vH+uX1vYiJW7ufvzJjVdDNP2jDzGXZZTrYH1K5Hv" +
                "i5ZRh9e+/OzIzrnZ5dXom9VgsmQHlgHvkSkiIZ1lIenjNGjUdAbeou1JXf6NwP+coIBSUFsLUEOsyEZsZCHMxp3k29CT5CxV" +
                "A2zNorMgCQ4cNAxCziK0Bs1UdiqP3//rskY1toO07d9CY0LBxo8RsrrPIjFBS1qSr6tV9YdSoDZZTyP0HD/KmEHmPl9iwLyY" +
                "xW1a23pGUe3q4Ybhr1KphnnPFVVfmzJ2LxXRt04sjvAYAd/zSUb1mDcdSH6E+cRJa16eBonA/CTJNwieiJT5y7KhxpRLe2D5" +
                "Rq3v0nm6Gry9m2Ijh1Feg35gxa2Y8rOQFWMYNAeLbKG2RBgkHKcVKFB81+sX1Gza4UNVEfJfkojfeepOE6S73dO18T1f8Dh7" +
                "y9MuvvjJvwXxiz7aDSKKdu3dNmDgRG02UjFGsC/J6MCUlqlSl8rbtbLlJt/+1beuxlOREUafGyuwWco4ZTGLTukmyK8E9RFW" +
                "/4mcOz4blYRgyOGcK68Eo3E+W7IZhFiW2NGNGj1ZaiGfuaCmebcYWB6220O133gHM8Y3C4LFFiOZ8f4qX063bt11W3fV+int" +
                "d0GtmT4nNb71l7IRxv//xh0HwCWn9xg1TXptKI1JcFtdBACQx/HIZpB8WLV4MkZuIRolfA36j34fwxWYyYPBtMB0C9BgtEfl" +
                "lFezee+91wmzUzhjWg1G4/xuC3Am9JEJ9GEyHNDyOGCsMmL2niej1N99QYnyGPxs0arj/4AFHrLWurlcXiV6BO56PZXykgwh" +
                "Vl1a89Pobrr/zrtY9evXs0b1H165d7+3Zs327dg0aN6xcpXJ2MYU3ZDYW4ZksNQlRH/thxY8o291dOj8pHusTtPtSJTEI0pP" +
                "R7cN0UYxs9OSRI9kj2plROKZHUbifIkHiNNuL6PdTWXsCDwZjC2l+GwfAVsVKFSHAJCenKOGaBov0S6z0nwPsJcaRbUo5cuf" +
                "CVZ6b+n1ZxIkFGC0/UBIhFIUTZI9YLH7JSbzYG+NFoPvuu2+PeKvbtZe1/ohmw+RR+QsWwEdx90j7OekdyMD1IB8F7zFnUky" +
                "PSFG4nzqlaBt0lkEFlJWrVlHCaAF3iLN285sUI9jUb9jA0ftZc+bKOW/hguIlirNooXXhNWvXItEZoN/456YH+/U1Chwm0cH" +
                "j4bbCEQXAYc69oq23IZ4tR/ZOne+GC0sQfQ4uffLZp8gfI0pGtlmPE3vPE2Hd7tixEl82Tz5eU0ONHWc31hmgKNz/FYFRUVs" +
                "eOMCyB1GVKox4HsEtnKH5ccIg0NFDwebz5stb58orRr/EK+pBLd4oCdxH0AQ6CTckWvwicZQcUQrNmD2zc9cuVS+raqT5DFL" +
                "58uXvvPPOmTNmmLVbEF2iycChI4eVdv2FboMY5QD6CbEeo/e4ULY4sYrJkzs3nm90jlHu/t8mM0bjZMYsDouHXUsRwWEn4hx" +
                "ApHux7NpTjL3gtQYSTgEdUV6Jo4H3p31gYAr6bfXq2R/PIdHoiUEDBwx8onWbu+5q24ZOHn7s0VGjX/xwxkc/iXNJm5IDgcO" +
                "yNvzquLGUAQ8vVLhQxgX0kAM9WYmXEb98FOR11IxdUaE1eKYoCvdMI+yZwMwVvj/R6sCB+bXBYVJwrsR6LE++vPQE7P1TYv+" +
                "4beeO/AVcl+2JSYkI5QW6/c477r2v14czZsxfuIAYc1o8pxIsdZd983WHDh22b98+UPy8Llq8uFYdDs5coFBBtk9GsDQ99zg" +
                "1uIO1o4vWqlnL0T64g3okPItYD0bhnll0TAgnycLDGkvAMHBKoNnM9jDWG4jggA0WTzfF1BE2g2vWrXUs8WbI0CEIQQMaNmL" +
                "4dTdcj/P2HTs0a+5a8363/HvYpnft3u0jicBKr0YAAiISmHBCXF9p/9Q+2RXOooi1BJtxxCObT4ffoMeWq3CJI9tiAnoDSvB" +
                "sYz0YhfvpIG5jmcUumD8/f8EC7FySMKFhDfVfRMSYQ4kMQzdWrFKZ+TrNYmU/6PBnRgCpZcpetHX7tnx6zjprzmzsI1GyNbt" +
                "o8WIMuEvKww+MstwitGnbpmjxoh69PwNyucGrDVw78YRHnFhB8q/Xm5iYiMhTgDjIyDNnl6Jwz3yC8sHsM1KW/y2/6E/CjSj" +
                "DD3QMJUpJ7gAeVb2mu4bf5Z6uu/fuwXnW7Nm279xh9hOtXb8O+vVs2bPBF7ESNwEEfbBe6jaY+6ZnoHsKB3aoYP1YWUGSQ+v" +
                "lHKAo3DOZMBszGjc6J2gCBwAcL76kb1JmDt//2zt7niiCMI4fcnceifoBaNVCLYwSE6VQG5DmbIhvsUCNPV/ARhJoJKJ+AF8" +
                "IVvAB1EQ+BrWhJXRIAezizPxvnhv3UIbJsTwH/182l7vdW3aX/e3cszPPzPp+fUhzt71XXW2gTXl3BTMuhncf3mNoYvPehvu" +
                "u1xzmY13zd85fvFDx43jZtfpsB+puuV4LbjwMv1x2A1Loiv8aBVD37iOtrSakkTSVLwvzGCbytO/8sW/A0Oc8RjGMdk2MylR" +
                "1bTc2KHJm13wKQAUJXq4qsOLi9YYbLMBG4T5Zzax+5txZlMcJE3Y73HNcw2Zzb9/ZhiS4jhYJDdFLAep+yPgzDhVQBMJjuPL" +
                "/wKbTrc767/aiYE7By/CNfO2gE1asu6QDc8E03M8U6ovW11tV+FK5Tt1PNCjwDDd8CN5yyLc99e/VTUTVhN1rNSH5PIhLVy7" +
                "jcQkIYKD4ro56mE6oe0lkOxmehLpr88s3Xk9NwXgU8zVvfKdkSib5TbBhlRsaZGJiYm1tDWabyA23K6iB0Vm059S9BOTEb21" +
                "v7+zaD3ZQFZ9ECUT6Pafk8CN+Kmyic4tV3Dr7hIVXr1ojJuz6PLkwW06n6zl1LwecfolnzMffG7a2zigy+2YWT3OXGptCYCN" +
                "hd8G/Lk7VoJcGYhVpYLLjbbhW0pq7FR4aGpqZnkHipPgtBxi+6oS6lwdKPlEEAQDmzH9dkJJeUg9gnhjZqWm3ptDvWhCj20X" +
                "B8B4ycB8OQW31y3+g7uWReXI/gAfeSIPU3NzcxPNn/b56EWM1/qsVtouT/IAgpkIug3ThGx8fn5me3tzchNwo1OVwCseoHOp" +
                "eEtnfWZOYA9AghflAsl8qvlW1LWXHm5ayHRKHq7jXva+ZVn6vm+oDjfBJqDeHb+W+VjF37cRS9xK+9hAZdS8NkaNgiXif+0H" +
                "5wPLy8tLS0uDgoHhfwYBe5pbRPQGm5mJrW5VpqDtrnbuIgtpltllkl5vPrWcX4wu4kMzc1pADjkaj8fHzp28/vm/5vC7Ut6B" +
                "CvbDbhTn6oe7qkOyDtvc/lx8/fXL1+jUMJN2mr3LqdP1UvWZeq75ux4RA6IWE7nlVt7S/YRYMVF2AjnQuu7qvZjE3oMPDw81" +
                "mc3FxUTaaOzkk6MLHcD97keOju5SRPQ2ORcKbMLIHq6urk5OTIyMj95vNsXtjofzx3L5758Gjh6P3Rp+/fLGyshL+fVuK72R" +
                "m23mW7/jdyI+6F1K3yI6H7lkQ+JLDo9eNp+7kALR/hrpH8VweJtkx0D2j671MmcZTd3LEUPeDUdAdM8M5pBz+Pi37IGtR94N" +
                "R0B0RYTiHlAP+85GEaxXP6KGRUXfSLeLFpe7pUHclxItL3dOh7kqIF5e6p0PdlRAvLnVPh7orIV5c6p4OdVdCvLjUPR3qroR" +
                "4cal7OtRdCfHiUvd0qLsS4sWl7ulQdyXEi0vd06HuSogXl7qnQ92VEC8udU+HuishXlzqng51V0K8uNQ9HequhHhxqXs61F0" +
                "J8eJS93SouxLixaXu6VB3JcSLS93Toe5KiBeXuqdD3ZUQLy51T4e6KyFeXOqeDnVXQry41D0d6q6EeHGpezrUXQnx4lL3dKi" +
                "7EuLFpe7pUHclxItL3dOh7kqIF/codQ92mJBjDnUnJ4g/1xPHIARwu8QAAAAASUVORK5CYII=";
        this.WIDTH = 1200;
        this.HEIGHT = 600;
        this.black = Color.BLACK;
        this.white = Color.WHITE;
        this.green = Color.GREEN;
        this.yellow = Color.YELLOW;
        this.red = Color.RED;
        this.blue = Color.BLUE;
        this.gray = Color.GRAY;
        this.orange = Color.ORANGE;
        this.backColor = black;
        this.frontColor = green;
    }

    public String getAppDir() {
        return this.appDir;
    }

    public void setAppDir(String appDir) {
        this.appDir = appDir;
    }

    public String getIdGeneratorFileName() {
        return this.idGeneratorFileName;
    }

    public void setIdGeneratorFileName(String idGeneratorFileName) {
        this.idGeneratorFileName = idGeneratorFileName;
    }

    public String getSettingsFileName() {
        return this.settingsFileName;
    }

    public void setSettingsFileName(String settingsFileName) {
        this.settingsFileName = settingsFileName;
    }

    public String getCustomerFileName() {
        return this.customerFileName;
    }

    public void setCustomerFileName(String customerFileName) {
        this.customerFileName = customerFileName;
    }

    public String getEventFileName() {
        return this.eventFileName;
    }

    public void setEventFileName(String eventFileName) {
        this.eventFileName = eventFileName;
    }

    public String getPartFileName() {
        return this.partFileName;
    }

    public void setPartFileName(String partFileName) {
        this.partFileName = partFileName;
    }

    public String getPhoneNumFileName() {
        return this.phoneNumFileName;
    }

    public void setPhoneNumFileName(String phoneNumFileName) {
        this.phoneNumFileName = phoneNumFileName;
    }

    public String getVehicleFileName() {
        return this.vehicleFileName;
    }

    public void setVehicleFileName(String vehicleFileName) {
        this.vehicleFileName = vehicleFileName;
    }

    public String getFirstSeparator() {
        return this.firstSeparator;
    }

    public void setFirstSeparator(String firstSeparator) {
        this.firstSeparator = firstSeparator;
    }

    public String getSecondSeparator() {
        return this.secondSeparator;
    }

    public void setSecondSeparator(String secondSeparator) {
        this.secondSeparator = secondSeparator;
    }

    public String getThirdSeparator() {
        return this.thirdSeparator;
    }

    public void setThirdSeparator(String thirdSeparator) {
        this.thirdSeparator = thirdSeparator;
    }

    public String getFontCode() {
        return this.fontCode;
    }

    public void setFontCode(String fontCode) {
        this.fontCode = fontCode;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageString() {
        return this.imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public int getWIDTH() {
        return this.WIDTH;
    }

    public void setWIDTH(int WIDTH) {
        this.WIDTH = WIDTH;
    }

    public int getHEIGHT() {
        return this.HEIGHT;
    }

    public void setHEIGHT(int HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public Color getBlack() {
        return this.black;
    }

    public void setBlack(Color black) {
        this.black = black;
    }

    public Color getWhite() {
        return this.white;
    }

    public void setWhite(Color white) {
        this.white = white;
    }

    public Color getGreen() {
        return this.green;
    }

    public void setGreen(Color green) {
        this.green = green;
    }

    public Color getYellow() {
        return this.yellow;
    }

    public void setYellow(Color yellow) {
        this.yellow = yellow;
    }

    public Color getRed() {
        return this.red;
    }

    public void setRed(Color red) {
        this.red = red;
    }

    public Color getBlue() {
        return this.blue;
    }

    public void setBlue(Color blue) {
        this.blue = blue;
    }

    public Color getGray() {
        return this.gray;
    }

    public void setGray(Color gray) {
        this.gray = gray;
    }

    public Color getOrange() {
        return this.orange;
    }

    public void setOrange(Color orange) {
        this.orange = orange;
    }

    public Color getBackColor() {
        return this.backColor;
    }

    public void setBackColor(Color backColor) {
        this.backColor = backColor;
    }

    public Color getFrontColor() {
        return this.frontColor;
    }

    public void setFrontColor(Color frontColor) {
        this.frontColor = frontColor;
    }

    public Font getFont(){
        return new Font(fontCode.split(thirdSeparator)[0], Integer.parseInt(fontCode.split(thirdSeparator)[1]), Integer.parseInt(fontCode.split(thirdSeparator)[2]));
    }
}
