package com.yves.ph.FileManager;

import com.yves.ph.Settings.Settings;
import com.yves.ph.Settings.SettingsApp;
import com.yves.ph.IdGenerator.IdGenerator;
import com.yves.ph.Models.*;

import java.util.List;

public interface FileManage {

    public void saveSettingsFile();

    public Settings getAppSettings();

    public void saveIdGeneratorFile(IdGenerator idGenerator);

    public IdGenerator getIdGeneratorFile(Settings settings);

    public void saveCustomerFile(Settings settings, List<Customer> customers);

    public List<Customer> getCustomerRepo(Settings settings);

    public void saveEventFile(Settings settings, List<Event> events);

    public List<Event> getEventRepo(Settings settings);

    public void savePartFile(Settings settings, List<Part> parts);

    public List<Part> getPartRepo(Settings settings);

    public void savePhoneNumberFile(Settings settings, List<PhoneNumber> phoneNumbers);

    public List<PhoneNumber> getPhoneNumberRepo(Settings settings);

    public void saveVehicleFile(Settings settings, List<Vehicle> vehicles);

    public List<Vehicle> getVehicleRepo(Settings settings);
}
