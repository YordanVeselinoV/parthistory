package com.yves.ph.FileManager;

import com.yves.ph.Settings.Settings;
import com.yves.ph.Settings.SettingsApp;
import com.yves.ph.IdGenerator.IdGenerator;
import com.yves.ph.Models.*;
import com.yves.ph.Models.Event;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileManager implements FileManage {

    private FileWriter fw;
    private BufferedReader br;
    private Settings settings;

    public FileManager() {
        this.settings = new SettingsApp();
        appStartUpFilesCheck(settings);
    }

    private void appStartUpFilesCheck(Settings settings) {
        try {
            File appDir = new File(settings.getAppDir());
            if (!appDir.exists()) {
                appDir.mkdirs();
            }

            File appSettingsFile = new File(appDir, settings.getSettingsFileName());
            if (!appSettingsFile.exists()) {
                saveSettingsFile();
            }

            File idGeneratorFile = new File(appDir, settings.getIdGeneratorFileName());
            if (!idGeneratorFile.exists()) {
                saveIdGeneratorFile(new IdGenerator());
            }

            File customerRepoFile = new File(appDir, settings.getCustomerFileName());
            if (!customerRepoFile.exists()) {
                saveCustomerFile(settings, new ArrayList<>());
            }

            File eventRepoFile = new File(appDir, settings.getEventFileName());
            if (!eventRepoFile.exists()) {
                saveEventFile(settings, new ArrayList<>());
            }

            File partRepoFile = new File(appDir, settings.getPartFileName());
            if (!partRepoFile.exists()) {
                savePartFile(settings, new ArrayList<>());
            }

            File phoneNumRepoFile = new File(appDir, settings.getPhoneNumFileName());
            if (!phoneNumRepoFile.exists()) {
                savePhoneNumberFile(settings, new ArrayList<>());
            }

            File vehicleRepoFile = new File(appDir, settings.getVehicleFileName());
            if (!vehicleRepoFile.exists()) {
                saveVehicleFile(settings, new ArrayList<>());
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    public void saveSettingsFile() {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getSettingsFileName());
            fw.write(settings.getAppDir() + "\n");
            fw.write(settings.getSettingsFileName() + "\n");
            fw.write(settings.getIdGeneratorFileName() + "\n");
            fw.write(settings.getCustomerFileName() + "\n");
            fw.write(settings.getEventFileName() + "\n");
            fw.write(settings.getPartFileName() + "\n");
            fw.write(settings.getPhoneNumFileName() + "\n");
            fw.write(settings.getVehicleFileName() + "\n");
            fw.write(settings.getFirstSeparator() + "\n");
            fw.write(settings.getSecondSeparator() + "\n");
            fw.write(settings.getThirdSeparator() + "\n");
            fw.write(settings.getFontCode() + "\n");
            fw.write(settings.getTitle() + "\n");
            fw.write(settings.getImageString() + "\n");
            fw.write(settings.getWIDTH() + "\n");
            fw.write(settings.getHEIGHT() + "\n");
            fw.write(settings.getBlack().getRGB() + "\n");
            fw.write(settings.getWhite().getRGB() + "\n");
            fw.write(settings.getGreen().getRGB() + "\n");
            fw.write(settings.getYellow().getRGB() + "\n");
            fw.write(settings.getRed().getRGB() + "\n");
            fw.write(settings.getBlue().getRGB() + "\n");
            fw.write(settings.getGray().getRGB() + "\n");
            fw.write(settings.getOrange().getRGB() + "\n");
            fw.write(settings.getBackColor().getRGB() + "\n");
            fw.write(settings.getFrontColor().getRGB() + "\n");
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    public Settings getAppSettings() {
        SettingsApp settings = new SettingsApp();
        try {
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getSettingsFileName()));
            settings.setAppDir(br.readLine());
            settings.setSettingsFileName(br.readLine());
            settings.setIdGeneratorFileName(br.readLine());
            settings.setCustomerFileName(br.readLine());
            settings.setEventFileName(br.readLine());
            settings.setPartFileName(br.readLine());
            settings.setPhoneNumFileName(br.readLine());
            settings.setVehicleFileName(br.readLine());
            settings.setFirstSeparator(br.readLine());
            settings.setSecondSeparator(br.readLine());
            settings.setThirdSeparator(br.readLine());
            settings.setFontCode(br.readLine());
            settings.setTitle(br.readLine());
            settings.setImageString(br.readLine());
            settings.setWIDTH(Integer.parseInt(br.readLine()));
            settings.setHEIGHT(Integer.parseInt(br.readLine()));
            settings.setBlack(Color.decode(br.readLine()));
            settings.setWhite(Color.decode(br.readLine()));
            settings.setGreen(Color.decode(br.readLine()));
            settings.setYellow(Color.decode(br.readLine()));
            settings.setRed(Color.decode(br.readLine()));
            settings.setBlue(Color.decode(br.readLine()));
            settings.setGray(Color.decode(br.readLine()));
            settings.setOrange(Color.decode(br.readLine()));
            settings.setBackColor(Color.decode(br.readLine()));
            settings.setFrontColor(Color.decode(br.readLine()));
            this.settings = settings;
            br.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return this.settings;
    }

    @Override
    public void saveIdGeneratorFile(IdGenerator idGenerator) {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getIdGeneratorFileName());
            fw.write(idGenerator.getLastCustomerId() + "\n");
            fw.write(idGenerator.getLastEventId() + "\n");
            fw.write(idGenerator.getLastPartId() + "\n");
            fw.write(idGenerator.getLastPhoneNumId() + "\n");
            fw.write(idGenerator.getLastVehicleId() + "\n");
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    @Override
    public IdGenerator getIdGeneratorFile(Settings settings) {
        IdGenerator idGenerator = new IdGenerator();
        try {
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getIdGeneratorFileName()));
            idGenerator.setLastCustomerId(Long.parseLong(br.readLine()));
            idGenerator.setLastEventId(Long.parseLong(br.readLine()));
            idGenerator.setLastPartId(Long.parseLong(br.readLine()));
            idGenerator.setLastPhoneNumId(Long.parseLong(br.readLine()));
            idGenerator.setLastVehicleId(Long.parseLong(br.readLine()));
            br.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return idGenerator;
    }

    @Override
    public void saveCustomerFile(Settings settings, List<Customer> customers) {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getCustomerFileName());
            for (int i = 0; i < customers.size(); i++) {
                Customer customer = customers.get(i);
                fw.write(customer.getId() + settings.getFirstSeparator() +
                        customer.getName() + settings.getFirstSeparator());
                List<String> phoneNumIds = customer.getPhoneNumIds();
                for (int j = 0; j < phoneNumIds.size(); j++) {
                    fw.write(phoneNumIds.get(j));
                    if (j < phoneNumIds.size() - 1) {
                        fw.write(settings.getSecondSeparator());
                    }
                }
                fw.write(settings.getFirstSeparator());
                List<String> vehicleIds = customer.getVehicleIds();
                for (int k = 0; k < vehicleIds.size(); k++) {
                    fw.write(vehicleIds.get(k));
                    if (k < vehicleIds.size() - 1) {
                        fw.write(settings.getSecondSeparator());
                    }
                }
                fw.write("\n");
            }
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    @Override
    public List<Customer> getCustomerRepo(Settings settings) {
        List<Customer> customers = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getCustomerFileName()));
            while (true) {
                String line = br.readLine();
                if (line == null) break;
                if (line.split(settings.getFirstSeparator()).length == 4) {
                    Customer customer = new Customer(line.split(settings.getFirstSeparator())[0]);
                    customer.setName(line.split(settings.getFirstSeparator())[1]);
                    String phoneNumIds = line.split(settings.getFirstSeparator())[2];
                    String[] phoneNumsArray = phoneNumIds.split(settings.getSecondSeparator());
                    for (String phoneNumId : phoneNumsArray) {
                        customer.getPhoneNumIds().add(phoneNumId);
                    }
                    String vehicleIds = line.split(settings.getFirstSeparator())[3];
                    String[] vehiclesArray = vehicleIds.split(settings.getSecondSeparator());
                    for (String vehicleId : vehiclesArray) {
                        customer.getVehicleIds().add(vehicleId);
                    }
                    customers.add(customer);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return customers;
    }

    @Override
    public void saveEventFile(Settings settings, List<Event> events) {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getEventFileName());
            for (int i = 0; i < events.size(); i++) {
                Event event = events.get(i);
                fw.write(event.getId() + settings.getFirstSeparator() +
                        event.getDate() + settings.getFirstSeparator());
                List<String> partIds = event.getPartIds();
                for (int j = 0; j < partIds.size(); j++) {
                    fw.write(partIds.get(j));
                    if (j < partIds.size() - 1) {
                        fw.write(settings.getSecondSeparator());
                    }
                }
                fw.write("\n");
            }
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    @Override
    public List<Event> getEventRepo(Settings settings) {
        List<Event> events = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getEventFileName()));
            while (true) {
                String line = br.readLine();
                if (line == null) break;
                if (line.split(settings.getFirstSeparator()).length == 3) {
                    Event event = new Event(line.split(settings.getFirstSeparator())[0]);
                    event.setDate(line.split(settings.getFirstSeparator())[1]);
                    String eventIds = line.split(settings.getFirstSeparator())[2];
                    String[] eventsArray = eventIds.split(settings.getSecondSeparator());
                    for (String eventId : eventsArray) {
                        event.getPartIds().add(eventId);
                    }
                    events.add(event);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return events;
    }

    @Override
    public void savePartFile(Settings settings, List<Part> parts) {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getPartFileName());
            for (Part part : parts) {
                fw.write(part.getId() + settings.getFirstSeparator() +
                        part.getPartNum() + settings.getFirstSeparator() +
                        part.getPartName() + "\n");
            }
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    @Override
    public List<Part> getPartRepo(Settings settings) {
        List<Part> parts = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getPartFileName()));
            while (true) {
                String line = br.readLine();
                if (line == null) break;
                if (line.split(settings.getFirstSeparator()).length == 3) {
                    Part part = new Part(line.split(settings.getFirstSeparator())[0]);
                    part.setPartNum(line.split(settings.getFirstSeparator())[1]);
                    part.setPartName(line.split(settings.getFirstSeparator())[2]);
                    parts.add(part);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return parts;
    }

    @Override
    public void savePhoneNumberFile(Settings settings, List<PhoneNumber> phoneNumbers) {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getPhoneNumFileName());
            for (PhoneNumber phoneNumber : phoneNumbers) {
                fw.write(phoneNumber.getId() + settings.getFirstSeparator() +
                        phoneNumber.getNumber() + "\n");
            }
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    @Override
    public List<PhoneNumber> getPhoneNumberRepo(Settings settings) {
        List<PhoneNumber> phoneNumbers = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getPartFileName()));
            while (true) {
                String line = br.readLine();
                if (line == null) break;
                if (line.split(settings.getFirstSeparator()).length == 2) {
                    PhoneNumber phoneNumber = new PhoneNumber(line.split(settings.getFirstSeparator())[0]);
                    phoneNumber.setNumber(line.split(settings.getFirstSeparator())[1]);
                    phoneNumbers.add(phoneNumber);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return phoneNumbers;
    }

    @Override
    public void saveVehicleFile(Settings settings, List<Vehicle> vehicles) {
        try {
            fw = new FileWriter(settings.getAppDir() + "\\" + settings.getVehicleFileName());
            for (int i = 0; i < vehicles.size(); i++) {
                Vehicle vehicle = vehicles.get(i);
                fw.write(vehicle.getId() + settings.getFirstSeparator() +
                        vehicle.getVehicleInfo() + settings.getFirstSeparator());
                List<String> eventIds = vehicle.getEventIds();
                for (int j = 0; j < eventIds.size(); j++) {
                    fw.write(eventIds.get(j));
                    if (j < eventIds.size() - 1) {
                        fw.write(settings.getSecondSeparator());
                    }
                }
                fw.write("\n");
            }
            fw.flush();
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
    }

    @Override
    public List<Vehicle> getVehicleRepo(Settings settings) {
        List<Vehicle> vehicles = new ArrayList<>();
        try{
            br = new BufferedReader(new FileReader(settings.getAppDir() + "\\" + settings.getPartFileName()));
            while (true) {
                String line = br.readLine();
                if (line == null) break;
                if (line.split(settings.getFirstSeparator()).length == 3) {
                    Vehicle vehicle = new Vehicle(line.split(settings.getFirstSeparator())[0]);
                    vehicle.setVehicleInfo(line.split(settings.getFirstSeparator())[1]);
                    String eventIds = line.split(settings.getFirstSeparator())[2];
                    String[] eventsArray = eventIds.split(settings.getSecondSeparator());
                    for (String eventId : eventsArray) {
                        vehicle.getEventIds().add(eventId);
                    }
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage() + " => " + Arrays.toString(ex.getStackTrace()));
        }
        return vehicles;
    }
}
