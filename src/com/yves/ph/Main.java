package com.yves.ph;

import com.yves.ph.AppManager.AppManager;
import com.yves.ph.FileManager.FileManage;
import com.yves.ph.FileManager.FileManager;
import com.yves.ph.Models.Customer;
import com.yves.ph.Models.Event;
import com.yves.ph.Settings.Settings;
import com.yves.ph.Settings.SettingsApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        new AppManager();

//        FileManage fileManager = new FileManager();
//        Settings settings = new SettingsApp();

//        Customer customer = new Customer("C1");
//        customer.setName("Yordan");
//        customer.getPhoneNumIds().add("T1");
//        customer.getPhoneNumIds().add("T2");
//        customer.getVehicleIds().add("V1");
//        List<Customer> customers = new ArrayList<>();
//        customers.add(customer);
//        Customer customer1 = new Customer("C2");
//        customer1.setName("Veselinov");
//        customer1.getPhoneNumIds().add("T3");
//        customer1.getPhoneNumIds().add("T4");
//        customer1.getVehicleIds().add("V2");
//        customer1.getVehicleIds().add("V3");
//        customers.add(customer1);
//        fileManager.saveCustomerFile(settings, customers);

//        List<Customer> customerRepo = fileManager.getCustomerRepo(settings);
//        for (Customer cust : customerRepo) {
//            List<String> phoneNumIds = cust.getPhoneNumIds();
//            List<String> vehicleIds = cust.getVehicleIds();
//            System.out.print(cust.getId() + " " + cust.getName() + " ");
//            for (String phoneId : phoneNumIds) {
//                System.out.print(phoneId + " ");
//            }
//            for (String vehicleId : vehicleIds) {
//                System.out.print(vehicleId + " ");
//            }
//            System.out.println();
//        }

//        Event event = new Event("E1");
//        event.setDate(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
//        event.getPartIds().add("P1");
//        event.getPartIds().add("P2");
//
//        Event event1 = new Event("E2");
//        event1.setDate("22.09.1982");
//        event1.getPartIds().add("P1");
//        event1.getPartIds().add("P3");
//
//        List<Event> events = new ArrayList<>();
//        events.add(event);
//        events.add(event1);
//
//        fileManager.saveEventFile(settings, events);

//        List<Event> eventRepo = fileManager.getEventRepo(settings);
//        for (Event sample : eventRepo) {
//            System.out.print(sample.getId() + " " + sample.getDate() + " ");
//            for (String partId : sample.getPartIds()) {
//                System.out.print(partId + " ");
//            }
//            System.out.println();
//        }


    }
}
