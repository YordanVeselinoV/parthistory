package com.yves.ph.Dto;

import com.yves.ph.Models.PhoneNumber;
import com.yves.ph.Models.Vehicle;

import java.util.List;

public class CustomerDto {

    private String customerId;
    private String customerName;
    private List<String> phoneNumIds;
    private List<PhoneNumber> phoneNumbers;
    private List<String> vehicleIds;
    private List<Vehicle> vehicles;

    public CustomerDto(){}

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<String> getPhoneNumIds() {
        return this.phoneNumIds;
    }

    public void setPhoneNumIds(List<String> phoneNumIds) {
        this.phoneNumIds = phoneNumIds;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return this.phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<String> getVehicleIds() {
        return this.vehicleIds;
    }

    public void setVehicleIds(List<String> vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public List<Vehicle> getVehicles() {
        return this.vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
