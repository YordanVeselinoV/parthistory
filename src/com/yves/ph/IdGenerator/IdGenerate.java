package com.yves.ph.IdGenerator;

public interface IdGenerate {

    public String generateNewCustomerId();

    public String generateNewEventId();

    public String generateNewPartId();

    public String generateNewPhoneNumId();

    public String generateNewVehicleId();
}
