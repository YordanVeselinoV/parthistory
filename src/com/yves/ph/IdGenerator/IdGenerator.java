package com.yves.ph.IdGenerator;

public class IdGenerator implements IdGenerate{

    private long lastCustomerId;
    private long lastEventId;
    private long lastPartId;
    private long lastPhoneNumId;
    private long lastVehicleId;

    public IdGenerator(){
        setLastCustomerId(0);
        setLastEventId(0);
        setLastPartId(0);
        setLastPhoneNumId(0);
        setLastVehicleId(0);
    }

    public long getLastCustomerId() {
        return this.lastCustomerId;
    }

    public void setLastCustomerId(long lastCustomerId) {
        this.lastCustomerId = lastCustomerId;
    }

    public long getLastEventId() {
        return this.lastEventId;
    }

    public void setLastEventId(long lastEventId) {
        this.lastEventId = lastEventId;
    }

    public long getLastPartId() {
        return this.lastPartId;
    }

    public void setLastPartId(long lastPartId) {
        this.lastPartId = lastPartId;
    }

    public long getLastPhoneNumId() {
        return this.lastPhoneNumId;
    }

    public void setLastPhoneNumId(long lastPhoneNumId) {
        this.lastPhoneNumId = lastPhoneNumId;
    }

    public long getLastVehicleId() {
        return this.lastVehicleId;
    }

    public void setLastVehicleId(long lastVehicleId) {
        this.lastVehicleId = lastVehicleId;
    }

    public String generateNewCustomerId(){
        return "C" + String.valueOf(lastCustomerId + 1);
    }

    public String generateNewEventId(){
        return "E" + String.valueOf(lastEventId + 1);
    }

    public String generateNewPartId(){
        return  "P" + String.valueOf(lastPartId + 1);
    }

    public String generateNewPhoneNumId(){
        return "T" + String.valueOf(lastPhoneNumId + 1);
    }

    public String generateNewVehicleId(){
        return "V" + String.valueOf(lastVehicleId);
    }
}
